﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VerifySignature
{
    public partial class VerifySignature : Form
    {
        public VerifySignature()
        {
            InitializeComponent();
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            var source = txtSource.Text;
            var key = txtKey.Text.Trim();

            txtEncrypt.Text = aesEncryptBase64(source, key);
            txtDecrypt.Text = string.Empty;
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            var encrypt = txtEncrypt.Text;
            var key = txtKey.Text.Trim();

            txtDecrypt.Text = aesDecryptBase64(encrypt, key);
        }

        public static string aesEncryptBase64(string source, string merchantKey)
        {
            string encrypt = "";
            try
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();

                byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(merchantKey));
                byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(merchantKey));

                aes.Key = key;
                aes.IV = iv;

                byte[] dataByteArray = Encoding.UTF8.GetBytes(source);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(dataByteArray, 0, dataByteArray.Length);
                        cs.FlushFinalBlock();
                        encrypt = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return encrypt;
        }

        public static string aesDecryptBase64(string source, string merchantKey)
        {
            string decrypt = "";
            try
            {
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();

                byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(merchantKey));
                byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(merchantKey));

                aes.Key = key;
                aes.IV = iv;

                byte[] dataByteArray = Convert.FromBase64String(source);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(dataByteArray, 0, dataByteArray.Length);
                        cs.FlushFinalBlock();
                        decrypt = Encoding.UTF8.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return decrypt;
        }
    }
}
