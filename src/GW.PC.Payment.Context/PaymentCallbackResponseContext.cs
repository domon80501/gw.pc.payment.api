﻿namespace GW.PC.Payment.Context
{
    public class PaymentCallbackResponseContext
    {
        public bool Succeeded { get; set; }
        public string Result { get; set; }
        public string Content { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMethod { get; set; }
        public string MerchantOrderNumber { get; set; }
        public string PaymentCenterOrderNumber { get; set; }
    }
}
