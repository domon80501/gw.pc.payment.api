﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;
using GW.PC.Payment.Manager;
using GW.PC.Payment;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using GW.PC.Payment.Context;
using GW.PC.Models;

namespace GW.PC.Payment.Api.Controllers
{
    public class DepositCallbacksController : ApiController
    {
        public async Task<PaymentCallbackResponseContext> Post(PaymentCallbackRequestContext requestContext)
        {
            using (SerilogLogger.Logger.PushProperty("RequestId", requestContext.RequestId))
            {
                SerilogLogger.Logger.LogInformation("Deposit callback received from api");

                try
                {
                    var manager = await new DepositManager().Init(requestContext.PaymentChannelId);

                    var result = await manager.ParseCallback(requestContext);

                    if (!(result is Deposit deposit))
                    {

                        if (result is PaymentCallbackResponse paymentCallbackResponseContext)
                        {
                            return new PaymentCallbackResponseContext
                            {
                                Succeeded = !(paymentCallbackResponseContext.PaymentCallbackResult == PaymentCallbackResult.Failed),
                                Content = paymentCallbackResponseContext.Content,
                                Result = paymentCallbackResponseContext.PaymentCallbackResult.ToString()
                            };
                        }
                        else
                        {
                            throw new Exception("Error return value from deposit manager callback");
                        }
                    }

                    using (SerilogLogger.Logger.PushProperty("MerchantOrderNumber", deposit.PaymentCenterOrderNumber))
                    {

                        var response = (DepositCallbackResponse)(await manager.Callback(requestContext));

                        return new PaymentCallbackResponseContext
                        {
                            Succeeded = !(response.PaymentCallbackResult == PaymentCallbackResult.Failed),
                            Result = response.PaymentCallbackResult.ToString(),
                            Content = response.Content,
                            PaymentType = deposit.PaymentChannel.PaymentType.ToString(),
                            PaymentMethod = deposit.PaymentChannel.PaymentMethod.ToString(),
                            MerchantOrderNumber = deposit.MerchantOrderNumber,
                            PaymentCenterOrderNumber = deposit.PaymentCenterOrderNumber,
                        };
                    }
                }
                catch (Exception ex)
                {
                    return new PaymentCallbackResponseContext
                    {
                        Result = ex.Message,
                        Content = ex.ToString()
                    };
                }
            }
        }
    }
}