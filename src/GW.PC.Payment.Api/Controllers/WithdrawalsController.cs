﻿using GW.PC.Merchant.Gateway.Context;
using GW.PC.Payment.Manager;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Payment.Api.Controllers
{
    public class WithdrawalsController : ApiController
    {
        public async Task<MerchantQueryResponseContext> Get(MerchantQueryRequestContext requestContext)
        {
            var manager = await new WithdrawalManager().Init(requestContext);

            return await manager.Query();
        }

        public async Task<MerchantResponseContext> Post(MerchantWithdrawalRequestContext requestContext)
        {
            var manager = await new WithdrawalManager().Init(requestContext);

            return await manager.Request();
        }
    }
}
