﻿using GW.PC.Core;
using GW.PC.Models.Infrastructure;
using GW.PC.Payment.Manager;
using GW.PC.Payment;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using GW.PC.Payment.Context;
using GW.PC.Models;

namespace GW.PC.Payment.Api.Controllers
{
    public class WithdrawalCallbacksController : ApiController
    {
        public async Task<PaymentCallbackResponseContext> Post(PaymentCallbackRequestContext requestContext)
        {
            using (SerilogLogger.Logger.PushProperty("RequestId", requestContext.RequestId))
            {
                SerilogLogger.Logger.LogInformation("Withdrawal callback received from api");

                try
                {
                    var manager = await new WithdrawalManager().Init(requestContext.PaymentChannelId);

                    var response = (MerchantWithdrawalCallbackResponse)await manager.Callback(requestContext);

                    if (response is MerchantWithdrawalCallbackResponse merchantWithdrawalCallbackResponse)
                    {
                        var withdrawal = merchantWithdrawalCallbackResponse.MerchantWithdrawal;

                        return new PaymentCallbackResponseContext
                        {
                            Succeeded = !(response.PaymentCallbackResult == PaymentCallbackResult.Failed),
                            Result = response.PaymentCallbackResult.ToString(),
                            Content = response.Content,
                            PaymentType = withdrawal.PaymentChannel.PaymentType.ToString(),
                            PaymentMethod = withdrawal.PaymentChannel.PaymentMethod.ToString(),
                            MerchantOrderNumber = withdrawal.MerchantOrderNumber,
                            PaymentCenterOrderNumber = withdrawal.PaymentCenterOrderNumber,
                        };
                    }
                    else if (response is PaymentCallbackResponse paymentCallbackResponseContext)
                    {
                        return new PaymentCallbackResponseContext
                        {
                            Succeeded = !(paymentCallbackResponseContext.PaymentCallbackResult == PaymentCallbackResult.Failed),
                            Content = paymentCallbackResponseContext.Content,
                            Result = paymentCallbackResponseContext.PaymentCallbackResult.ToString()
                        };
                    }
                    else
                    {
                        throw new Exception("Error return value from withdrawal manager callback");
                    }
                }
                catch (Exception ex)
                {
                    return new PaymentCallbackResponseContext
                    {
                        Result = ex.Message,
                        Content = ex.ToString()
                    };
                }
            }
        }
    }
}