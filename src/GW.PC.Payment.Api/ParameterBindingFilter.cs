﻿using GW.PC.Core;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace GW.PC.Payment.Api
{
    public class ParameterBindingFilter : ActionFilterAttribute
    {
        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var arguments = actionContext.ActionDescriptor.GetParameters();

            if (arguments.Count == 1)
            {
                var argument = arguments.Single();

                object model = actionContext.ActionArguments[argument.ParameterName];

                if (actionContext.Request.Method == HttpMethod.Get)
                {
                    if (actionContext.Request.RequestUri.Query.Length > 1)
                    {
                        model = WebUtility.DeserializeQueryString(
                            Uri.UnescapeDataString(actionContext.Request.RequestUri.Query.Substring(1)),
                            argument.ParameterType);
                    }
                }
                else if (actionContext.Request.Method == HttpMethod.Post)
                {
                    if (actionContext.Request.Content.Headers.ContentType.MediaType == "application/json")
                    {
                        // Unit test
                        model = actionContext.ActionArguments[argument.ParameterName];
                    }
                    else if (actionContext.Request.Content.Headers.ContentType.MediaType == "application/x-www-form-urlencoded")
                    {
                        // Front end
                        model = WebUtility.DeserializeQueryString(
                            Uri.UnescapeDataString(await actionContext.Request.Content.ReadAsStringAsync()),
                            argument.ParameterType);
                    }
                }

                actionContext.Request.Properties.TryGetValue("guid", out object guid);

                SerilogLogger.Logger.LogInformation($"{guid?.ToString()} Headers: {JsonConvert.SerializeObject(actionContext.Request.Headers)}");

                if (model is WebRequestModel webRequestModel)
                {
                    if (actionContext.Request.Headers.TryGetValues("x-real-ip", out var values))
                    {
                        webRequestModel.IP = values.FirstOrDefault();
                    }
                }

                actionContext.ActionArguments[argument.ParameterName] = model;
            }

            await base.OnActionExecutingAsync(actionContext, cancellationToken);
        }
    }
}