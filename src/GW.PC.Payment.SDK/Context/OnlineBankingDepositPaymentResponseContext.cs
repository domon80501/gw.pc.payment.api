﻿using System;

namespace GW.PC.Payment.SDK
{
    [Serializable]
    public class OnlineBankingDepositPaymentResponseContext : PaymentResponse
    {
        public virtual string CardNumber { get; set; }
        public virtual string PayeeName { get; set; }
        public virtual string Remark { get; set; }
        public virtual string Email { get; set; }
    }
}
