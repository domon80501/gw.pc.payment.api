﻿using System;
using System.Collections.Generic;

namespace GW.PC.Payment.SDK
{
    public class PaymentGatewayRequestContext : MarshalByRefObject
    {
        public HttpMethod HttpMethod { get; set; }
        public string Url { get; set; }
        public ContentType ContentType { get; set; } = ContentType.None;
        public object Content { get; set; }
        public string EncodingName { get; set; } = "UTF-8";
        public IDictionary<string, object> Data { get; set; }
    }

    public enum ContentType
    {
        None,
        Json,
        FormUrlEncoded,
        Stream,
        FormUrlEncodedKeyValue,
    }
}