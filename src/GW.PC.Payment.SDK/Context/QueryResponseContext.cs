﻿using Newtonsoft.Json;
using System;

namespace GW.PC.Payment.SDK
{
    public class QueryResponseContext : MarshalByRefObject
    {
        public QueryResponse Content { get; set; }

        public string ParseContent
        {
            get
            {
                return JsonConvert.SerializeObject(Content);
            }
        }
    }

    public abstract class QueryResponse : MarshalByRefObject
    {
        public virtual string Amount { get; set; }
        
        public virtual string PaymentPendingReason { get; set; }
    }
}
