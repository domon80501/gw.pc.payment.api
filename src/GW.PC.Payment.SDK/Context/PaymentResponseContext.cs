﻿using Newtonsoft.Json;
using System;

namespace GW.PC.Payment.SDK
{
    public class PaymentResponseContext : MarshalByRefObject
    {
        public PaymentResponse Content { get; set; }

        public string ParseContent
        {
            get
            {
                return JsonConvert.SerializeObject(Content);
            }
        }
    }

    public abstract class PaymentResponse : MarshalByRefObject
    {
        public virtual string PaymentOrderNumber { get; set; }
    }
}
