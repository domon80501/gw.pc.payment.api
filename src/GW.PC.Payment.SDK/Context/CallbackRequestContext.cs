﻿using Newtonsoft.Json;
using System;

namespace GW.PC.Payment.SDK
{
    public class CallbackRequestContext : MarshalByRefObject
    {
        public CallbackRequest Content { get; set; }

        public string ParseContent
        {
            get
            {
                return JsonConvert.SerializeObject(Content);
            }
        }
    }

    public abstract class CallbackRequest : MarshalByRefObject
    {
        public abstract string MerchantOrderNumber { get; set; }
        public virtual string PaymentOrderNumber { get; set; }
        public virtual string PaidAt { get; set; }
        public virtual string PayerName { get; set; }
        public virtual string Amount { get; set; }
    }
}