﻿using Newtonsoft.Json;
using System;

namespace GW.PC.Payment.SDK
{
    public class PaymentRequestContext : MarshalByRefObject
    {
        public string Url { get; set; }

        public PaymentRequest Content { get; set; }

        public string RequestContent
        {
            get
            {
                return JsonConvert.SerializeObject(Content);
            }
        }
    }

    public abstract class PaymentRequest : MarshalByRefObject
    {   
    }
}
