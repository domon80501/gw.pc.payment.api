﻿namespace GW.PC.Payment.SDK
{
    /// <summary>
    /// 支付方式
    /// </summary>
    public enum PayMethod
    {
        /// <summary>
        /// 微信
        /// </summary>
        WeChat = 1,
        /// <summary>
        /// 支付宝
        /// </summary>
        Alipay = 2,
        /// <summary>
        /// 在线支付
        /// </summary>
        OnlinePayment = 3,
        /// <summary>
        /// 网银支付
        /// </summary>
        OnlineBanking = 4,
        /// <summary>
        /// 充值卡
        /// </summary>
        PrepaidCard = 5,
        /// <summary>
        /// QQ钱包
        /// </summary>
        QQWallet = 6,
        /// <summary>
        /// 快捷支付（银联快捷）
        /// </summary>
        QuickPay = 7,
        /// <summary>
        /// 支付转卡
        /// </summary>
        OnlineToBankCard = 8,
        /// <summary>
        /// 京东钱包
        /// </summary>
        JDWallet = 9,
    }
}