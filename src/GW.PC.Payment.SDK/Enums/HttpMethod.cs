﻿namespace GW.PC.Payment.SDK
{
    public enum HttpMethod
    {
        None,
        Get,
        Post,
        Delete,
        Patch,
        Put,
    }
}
