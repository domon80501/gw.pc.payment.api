﻿namespace GW.PC.Payment.SDK
{
    public enum PayType
    {
        /// <summary>
        /// 存款
        /// </summary>
        Deposit = 1,
        /// <summary>
        /// 提款
        /// </summary>
        Withdrawal,        
    }
}
