﻿namespace GW.PC.Payment.SDK
{
    public enum PayPlatform
    {
        All = 0,
        PC,
        Mobile,
        App,
    }
}
