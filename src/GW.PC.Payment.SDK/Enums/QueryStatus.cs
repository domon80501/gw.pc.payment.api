﻿namespace GW.PC.Payment.SDK
{
    public enum QueryStatus
    {
        /// <summary>
        /// 支付中
        /// </summary>
        PendingPayment = 0,
        /// <summary>
        /// 成功
        /// </summary>
        Succeeded = 1,
        /// <summary>
        /// 失败
        /// </summary>
        Failed = 2,        
    }
}
