﻿using System;

namespace GW.PC.Payment.SDK
{
    public abstract class PaymentHandlerSDK : MarshalByRefObject, IPayment, ICallback, IQuery
    {
        public PaymentManagerSDK manager;

        public virtual int CreateOrderNumber() => 4;

        public PaymentResponse PaymentResponseContent { get; set; }

        public QueryResponse QueryResponseContent { get; set; }

        public CallbackRequest CallbackRequestContent { get; set; }

        #region Payment
        public abstract PaymentRequestContext CreatePaymentRequest();

        public virtual PaymentGatewayRequestContext GetPaymentResponse(PaymentRequestContext request)
        {
            return new PaymentGatewayRequestContext
            {
                Content = request.Content == null ? request.Url :
                $"{request.Url}?{request.Content.SerializeToQueryString()}"
            };
        }

        public virtual PaymentResponseContext ReadPaymentResponse(string response) => null;

        public virtual bool IsValidPaymentResponse() => true;

        public abstract bool IsSuccessfulPaymentResponse();

        public virtual string BuildPaymentResult(string response) => response;
        #endregion

        #region Callback
        public virtual CallbackRequestContext ReadCallbackRequest(string callback) => null;

        public virtual string ExtractCallbackMerchantOrderNumber()
            => CallbackRequestContent.MerchantOrderNumber;

        public abstract bool IsValidCallback(string merchantKey);

        public abstract bool IsSuccessfulCallback();

        public string GetCallbackErrorMessage(string callback) => callback;

        public virtual string CallbackSuccess() => "SUCCESS";
        #endregion

        #region Query
        public virtual bool SupportQuery() => true;
        public virtual PaymentRequestContext CreateQueryRequest() => null;

        public virtual PaymentGatewayRequestContext GetQueryResponse(PaymentRequestContext context) => GetPaymentResponse(context);

        public virtual QueryResponseContext ReadQueryResponse(string response) => null;

        public virtual bool IsValidQueryResponse() => false;

        public virtual QueryStatus ParseQueryResult() => QueryStatus.PendingPayment;

        public virtual string ParseFailedQueryMessage(string response) => response;
        #endregion
    }
}
