﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GW.PC.Payment.SDK
{
    public abstract class PaymentManagerSDK : MarshalByRefObject
    {
        #region parameters

        #region PaymentChannel
        /// <summary>
        /// 支付类型
        /// </summary>
        public virtual PayType PayType { get; }
        /// <summary>
        /// 支付方法
        /// </summary>
        public virtual PayMethod PayMethod { get; }
        /// <summary>
        /// 商户号
        /// </summary>
        public virtual string MerchantUsername { get; }
        /// <summary>
        /// 商户金钥
        /// </summary>
        public virtual string MerchantKey { get; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public virtual string RequestUrl { get; }
        /// <summary>
        /// 回调地址
        /// </summary>
        public virtual string CallbackUrl { get; }
        /// <summary>
        /// 查询地址
        /// </summary>
        public virtual string QueryUrl { get; }
        /// <summary>
        /// 设备平台
        /// </summary>
        public virtual PayPlatform PayPlatform { get; }

        public virtual string Arguments { get; }
        #endregion

        /// <summary>
        /// 请求金额
        /// </summary>
        public abstract decimal RequestedAmount { get; }
        /// <summary>
        /// 商户订单号
        /// </summary>
        public abstract string MerchantOrderNumber { get; }
        /// <summary>
        /// 商户端的使用者帐号
        /// </summary>
        public virtual string Username { get; }
        /// <summary>
        /// 商户端请求的初始文字
        /// </summary>
        public virtual string RequestRawText { get; }
        /// <summary>
        /// 商户端的使用者ip
        /// </summary>
        public virtual string PaymentIP { get; }
        /// <summary>
        /// 收款卡号
        /// </summary>
        public virtual string PayeeCardNumber { get; }
        /// <summary>
        /// 收款姓名
        /// </summary>
        public virtual string PayeeName { get; }
        /// <summary>
        /// 支付商银行编码
        /// </summary>
        public virtual string BankCode { get; }
        /// <summary>
        /// 支付商银行名称
        /// </summary>
        public virtual string BankName { get; }
        /// <summary>
        /// 付款姓名
        /// </summary>
        public virtual string PayerName { get; }
        /// <summary>
        /// 交易成功后，重定向的位址
        /// </summary>
        public virtual string RedirectionUrl { get; }
        /// <summary>
        /// 网银存款或支付转卡存款才会被实体化的列表
        /// </summary>
        public virtual IEnumerable<SDKBanks> BankList { get; set; }
        #endregion
    }

    public class SDKBanks
    {
        public string ProviderBankCode { get; set; }
        public string PaymentCenterBankCode { get; set; }
    }
}
