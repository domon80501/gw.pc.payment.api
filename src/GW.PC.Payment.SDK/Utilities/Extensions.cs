﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Net;

namespace GW.PC.Payment.SDK
{
    public static class SDKExtensions
    {
        public static string GetDisplayName(this Enum value)
        {
            if (value.GetType().GetCustomAttributes<FlagsAttribute>(false).Any())
            {
                var values = Enum.GetValues(value.GetType()).Cast<Enum>();
                var names = new List<string>();
                foreach (var v in values)
                {
                    if (value.HasFlag(v))
                    {
                        names.Add(GetSingleDisplayName(v));
                    }
                }

                return string.Join(";", names);
            }
            else
            {
                return GetSingleDisplayName(value);
            }

            string GetSingleDisplayName(Enum singleValue)
            {
                var fieldInfo = singleValue.GetType().GetField(singleValue.ToString());
                var attributes = fieldInfo?.GetCustomAttributes(typeof(DisplayAttribute), false);

                if (attributes?.Length > 0)
                {
                    return ((DisplayAttribute)attributes[0]).Name;
                }

                return fieldInfo?.Name;
            }
        }

        public static T FindEnumByDisplayName<T>(this string name)
        {
            var type = typeof(T);

            if (!type.IsEnum)
            {
                throw new InvalidOperationException();
            }

            foreach (var field in type.GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                    typeof(DisplayAttribute)) is DisplayAttribute attribute)
                {
                    if (attribute.Name == name)
                    {
                        return (T)field.GetValue(null);
                    }
                }
            }

            throw new ArgumentException($"Not found enum value with display name: {name}");
        }

        public static string ToDateString(this DateTime value, string format = SDKConstants.FormatStrings.DateFormat)
        {
            return value.ToString(format);
        }

        public static string ToMonthString(this DateTime value, string format = SDKConstants.FormatStrings.MonthFormat)
        {
            return value.ToString(format);
        }

        public static string ToYearString(this DateTime value, string format = SDKConstants.FormatStrings.YearFormat)
        {
            return value.ToString(format);
        }

        public static string ToDateTimeString(this DateTime value, string format = SDKConstants.FormatStrings.DateTimeFormat)
        {
            return value.ToString(format);
        }

        public static string ToUtcDateTimeString(this DateTime value)
        {
            return value.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff");
        }

        public static DateTime ToUTC8(this DateTime value)
        {
            return value.ToUniversalTime().AddHours(8);
        }

        public static string ToTimestamp(this DateTime value) =>
            (DateTime.Now.Subtract(new DateTime(1970, 1, 1)).Ticks / TimeSpan.TicksPerSecond).ToString();

        public static string SerializeToXml(this object data, string rootName = null, bool omitXmlDeclaration = false)
        {
            XmlSerializer serializer = null;
            if (string.IsNullOrEmpty(rootName))
            {
                serializer = new XmlSerializer(data.GetType());
            }
            else
            {
                serializer = new XmlSerializer(data.GetType(),
                    new XmlRootAttribute(rootName));
            }
            var ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            using (var ms = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(ms, new XmlWriterSettings
                {
                    // Must exclude BOM to fit for sportsbook requirement.
                    Encoding = new UTF8Encoding(false),
                    OmitXmlDeclaration = omitXmlDeclaration
                }))
                {
                    serializer.Serialize(writer, data, ns);

                    return Encoding.UTF8.GetString(ms.ToArray());
                }
            }
        }

        public static T DeserializeXml<T>(this string xml, string rootName = null)
            where T : class
        {
            XmlSerializer serializer = null;
            if (string.IsNullOrEmpty(rootName))
            {
                serializer = new XmlSerializer(typeof(T));
            }
            else
            {
                serializer = new XmlSerializer(typeof(T),
                    new XmlRootAttribute(rootName));
            }

            using (var sr = new StringReader(xml))
            {
                return serializer.Deserialize(sr) as T;
            }
        }

        public static string SerializeToQueryString(this object obj, string separator = "&", bool encode = false)
        {
            var result = new List<string>();

            foreach (var property in obj.GetType().GetProperties())
            {
                if (!property.CustomAttributes.Any(
                    a => a.AttributeType == typeof(JsonIgnoreAttribute)))
                {
                    var attr = property.GetCustomAttributes(false).FirstOrDefault(
                        a => a.GetType() == typeof(JsonPropertyAttribute))
                        as JsonPropertyAttribute;

                    result.Add(string.Format("{0}={1}",
                        attr == null ? property.Name : attr.PropertyName,
                        encode ? WebUtility.UrlEncode(property.GetValue(obj).ToString()) : property.GetValue(obj)));
                }
            }

            return string.Join(separator, result);
        }

        public static dynamic ParseXmlToDynamic(this string xml)
        {
            var doc = XDocument.Load(new StringReader(xml));
            dynamic result = new ExpandoObject();

            ParseXmlToDynamic(result, doc.Elements().First());

            return result;
        }

        private static void ParseXmlToDynamic(dynamic parent, XElement node)
        {
            if (node.HasElements)
            {
                if (node.Elements(node.Elements().First().Name.LocalName).Count() > 1)
                {
                    //list
                    var item = new ExpandoObject();
                    var list = new List<dynamic>();
                    foreach (var element in node.Elements())
                    {
                        ParseXmlToDynamic(list, element);
                    }

                    AddProperty(item, node.Elements().First().Name.LocalName, list);
                    AddProperty(parent, node.Name.ToString(), item);
                }
                else
                {
                    var item = new ExpandoObject();

                    foreach (var attribute in node.Attributes())
                    {
                        AddProperty(item, attribute.Name.ToString(), attribute.Value.Trim());
                    }

                    //element
                    foreach (var element in node.Elements())
                    {
                        ParseXmlToDynamic(item, element);
                    }

                    AddProperty(parent, node.Name.ToString(), item);
                }
            }
            else
            {
                AddProperty(parent, node.Name.ToString(), node.Value.Trim());
            }
        }

        private static void AddProperty(dynamic parent, string name, object value)
        {
            if (parent is List<dynamic>)
            {
                (parent as List<dynamic>).Add(value);
            }
            else
            {
                (parent as IDictionary<String, object>)[name] = value;
            }
        }
    }
}
