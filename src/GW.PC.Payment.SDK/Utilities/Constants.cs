﻿namespace GW.PC.Payment.SDK
{
    public static class SDKConstants
    {
        public static class FormatStrings
        {
            public const string MonthFormat = "yyyy/MM";
            public const string DateFormat = "yyyy/MM/dd";
            public const string YearFormat = "yyyy";
            public const string DateTimeFormat = "yyyy/MM/dd HH:mm:ss";
            public const string DateTimeRangeFormat = "yyyy/MM/dd HH:mm:ss";
            public const string DateTimeRangeValueString = "{0} - {1}";
            public const string DateTimeNoSecondFormat = "yyyy/MM/dd HH:mm";
        }

        public static class GatewayRequestDataItems
        {
            public const string Headers = "headers";
            public const string IgnoreServerCertificateValidation = "IgnoreServerCertificateValidation";
            public const string CertificateFilePath = "CertificateFilePath";
            public const string CertificatePassword = "CertificatePassword";
        }
    }
}
