﻿using System.Threading.Tasks;

namespace GW.PC.Payment.SDK
{
    interface IPayment
    {
        /// <summary>
        /// 建立支付请求(订单资讯)
        /// </summary>
        /// <returns></returns>
        PaymentRequestContext CreatePaymentRequest();
        /// <summary>
        /// 建立支付请求(Http包)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        PaymentGatewayRequestContext GetPaymentResponse(PaymentRequestContext context);
        /// <summary>
        /// 解析响应资料
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        PaymentResponseContext ReadPaymentResponse(string response);
        /// <summary>
        /// 验证资料正确性(验签)
        /// </summary>
        /// <returns></returns>
        bool IsValidPaymentResponse();
        /// <summary>
        /// 判断订单是否成功建立于第三方
        /// </summary>
        /// <returns></returns>
        bool IsSuccessfulPaymentResponse();
        /// <summary>
        /// 提取需求资料给客户端
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        string BuildPaymentResult(string response);
    }
}
