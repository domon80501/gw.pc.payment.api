﻿using System.Threading.Tasks;

namespace GW.PC.Payment.SDK
{
    interface IQuery
    {
        /// <summary>
        /// 建立查询请求(订单资讯)
        /// </summary>
        /// <returns></returns>
        PaymentRequestContext CreateQueryRequest();
        /// <summary>
        /// 建立查询请求(Http包)
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        PaymentGatewayRequestContext GetQueryResponse(PaymentRequestContext request);
        /// <summary>
        /// 解析响应资料
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        QueryResponseContext ReadQueryResponse(string response);
        /// <summary>
        /// 验证资料正确性(验签)
        /// </summary>
        /// <returns></returns>
        bool IsValidQueryResponse();
        /// <summary>
        /// 判断订单是否成功完成交易
        /// true: 成功
        /// false: 失败
        /// null: 支付中
        /// </summary>
        /// <returns></returns>
        QueryStatus ParseQueryResult();

         string ParseFailedQueryMessage(string response);
    }
}
