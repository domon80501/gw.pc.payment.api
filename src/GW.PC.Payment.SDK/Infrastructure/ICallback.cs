﻿namespace GW.PC.Payment.SDK
{
    interface ICallback
    {
        /// <summary>
        /// 解析回调的资料
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        CallbackRequestContext ReadCallbackRequest(string callback);
        /// <summary>
        /// 取得回调的商户号
        /// </summary>
        /// <returns></returns>
        string ExtractCallbackMerchantOrderNumber();
        /// <summary>
        /// 验证回调资料的正确性(验签)
        /// </summary>
        /// <param name="merchantKey"></param>
        /// <returns></returns>
        bool IsValidCallback(string merchantKey);
        /// <summary>
        /// 判断回调的订单是否成功完成交易
        /// </summary>
        /// <returns></returns>
        bool IsSuccessfulCallback();
        /// <summary>
        /// 取得回调订单的交易失败原因
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        string GetCallbackErrorMessage(string callback);
        /// <summary>
        /// 响应给第三方的指定字段
        /// </summary>
        /// <returns></returns>
        string CallbackSuccess();
    }
}
