﻿using GW.PC.Core;
using GW.PC.Models;
using GW.PC.Services.EF;
using GW.PC.Web.Core;
using System;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using GW.PC.Payment.SDK;
using GW.PC.Payment.Manager;
using GW.PC.Payment.Gateway.Api.Client;
using GW.PC.Merchant.Gateway.Context;
using System.Net;

namespace GW.PC.Payment
{
    public class HttpPaymentHandler : MarshalByRefObject
    {
        internal PaymentHandlerSDK plugin = null;
        internal HttpPaymentManager manager = null;
        internal AppDomain appDomain = null;

        protected HttpPayment payment = null;

        internal string EnsureMerchantOrderNumber() => RandomGenerator.NewMerchantOrderNumber(plugin.CreateOrderNumber());

        internal decimal GetCallbackAmount() => Convert.ToDecimal(plugin.CallbackRequestContent.Amount);

        internal virtual async Task Initialize(AppDomain appDomain, PaymentHandlerSDK plugin, HttpPaymentManager manager)
        {
            this.appDomain = appDomain;
            this.plugin = plugin;

            plugin.manager = this.manager = manager;

            payment = manager.BuildHttpPayment();

            if (payment != null)
            {
                await new HttpPaymentService().Add(payment);
            }
        }

        #region Payment
        /// <summary>
        /// Only to be called from HttpPaymentManager.
        /// </summary>
        /// <returns></returns>
        internal async Task<MerchantResponseContext> Request()
        {
            var response = string.Empty;

            try
            {
                var request = plugin.CreatePaymentRequest();
                await OnPaymentRequestSending(request);

                response = await GetPaymentResponse(request);
                await OnPaymentResponseReceived(response);

                return await ProcessPaymentResponse(response);
            }
            catch (Exception ex)
            {
                await OnPaymentException(ex);

                throw new RequestException(
                    string.Format(Constants.MessageTemplates.PluginFailure, manager.PaymentChannel.AssemblyName, manager.PaymentChannel.HandlerType, ex.Message),
                    NoticeType.PaymentCenter, HttpStatusCode.InternalServerError);
            }
            //finally
            //{
            //    AppDomain.Unload(appDomain);
            //}
        }

        private async Task OnPaymentRequestSending(PaymentRequestContext context)
        {
            await manager.OnPaymentRequestSending(context);
        }

        private async Task OnPaymentHttpRequestSending(PaymentGatewayRequestContext context)
        {
            await manager.OnPaymentHttpRequestSending(context);
        }

        private async Task OnPaymentResponseReceived(string response)
        {
            payment.Succeeded = true;
            payment.Response = response.GetSubstring(2000);
            payment.ResponseReceivedAt = DateTime.Now.ToE8();
            await new HttpPaymentService().Update(payment);

            await manager.OnPaymentResponseReceived(response);
        }

        private async Task OnPaymentResponseParse(PaymentResponseContext context)
        {
            await manager.OnPaymentResponseParse(context);
        }

        private async Task OnPaymentException(Exception ex)
        {
            payment.Succeeded = false;
            payment.Response = ex.Message;

            await new HttpPaymentService().Update(payment);
        }

        protected virtual async Task<string> GetPaymentResponse(PaymentRequestContext request)
        {
            var context = plugin.GetPaymentResponse(request);

            await OnPaymentHttpRequestSending(context);

            if (context.HttpMethod == HttpMethod.None)
            {
                return await Task.FromResult(context.Url);
            }

            var response = (await new PaymentGatewayClient().Request(context));

            return response.Result;
        }

        protected virtual async Task<MerchantResponseContext> ProcessPaymentResponse(string response)
        {
            var parseContext = plugin.ReadPaymentResponse(response);
            await OnPaymentResponseParse(parseContext);

            plugin.PaymentResponseContent = parseContext?.Content;

            if (plugin.IsValidPaymentResponse())
            {
                if (plugin.IsSuccessfulPaymentResponse())
                {
                    await manager.HandleSuccessfulPaymentResponse();
                }
                else
                {
                    await manager.HandleFailedPaymentResponse(response);
                }

                var result = plugin.BuildPaymentResult(response);

                return BuildResponseContext((
                    plugin.IsSuccessfulPaymentResponse() ? TransactionStatus.Success : TransactionStatus.Fail), NoticeType.PaymentProvider, result);

            }
            else
            {
                return BuildResponseContext(TransactionStatus.Fail, NoticeType.PaymentCenter
                    , string.Format(Constants.MessageTemplates.PluginSignFailure, manager.PaymentChannel.AssemblyName, manager.PaymentChannel.HandlerType)
                    );
            }
        }

        internal virtual MerchantResponseContext BuildResponseContext(TransactionStatus status, NoticeType noticeType, string content)
        {
            var context = new MerchantResponseContext
            {
                RequestStatus = RequestStatus.Success.ToInt32String(),
                NoticeType = noticeType.ToInt32String(),
                Status = status.ToInt32String(),
                Content = status == TransactionStatus.Success ? content : string.Empty,
                Message = status != TransactionStatus.Success ? content : string.Empty,
                MerchantOrderNumber = manager.RequestedMerchantOrderNumber,
                PaymentCenterOrderNumber = manager.MerchantOrderNumber,
                PaymentOrderNumber = plugin.PaymentResponseContent?.PaymentOrderNumber ?? ""
            };

            context.Sign = VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(context, string.Empty, string.Empty), manager.PaymentChannel.Merchant.MerchantKey);

            return context;
        }
        #endregion

        #region Callback
        //To distribute abstract and virtual method amongst SDK plugins.
        //The main flow is in the corresponding [*manager]
        #endregion

        #region Query
        /// <summary>
        /// Only to be called from HttpPaymentManager.
        /// </summary>
        /// <returns></returns>
        internal virtual async Task<MerchantDepositQueryResponseContext> Query()
        {
            try
            {
                if (plugin.SupportQuery())
                {
                    var response = string.Empty;

                    var request = plugin.CreateQueryRequest();
                    await OnQueryRequestSending(request);

                    response = await GetQueryResponse(request);
                    await OnQueryResponseReceived(response);

                    return await ProcessQueryResponse(response);
                }
            }
            catch (Exception ex)
            {
                await OnQueryException(ex);

                throw new RequestException(
                    string.Format(Constants.MessageTemplates.PluginFailure, manager.PaymentChannel.AssemblyName, manager.PaymentChannel.HandlerType, ex.Message),
                    NoticeType.PaymentCenter, HttpStatusCode.InternalServerError);
            }
            finally
            {
                AppDomain.Unload(appDomain);
            }

            return BuildQueryResponseContext(TransactionStatus.UnKnow, NoticeType.PaymentCenter
                   , Constants.Messages.PaymentChannelQueryNotSupported
                   );
        }

        protected virtual async Task<string> GetQueryResponse(PaymentRequestContext request)
        {
            var context = plugin.GetQueryResponse(request);
            await OnPaymentHttpRequestSending(context);

            if (context.HttpMethod == HttpMethod.None)
            {
                //return await Task.FromResult(context.Content.ToString());
                return await Task.FromResult(context.Url);
            }

            var response = (await new PaymentGatewayClient().Request(context));

            return response.Result;
        }

        private async Task<MerchantDepositQueryResponseContext> ProcessQueryResponse(string response)
        {
            var parseContext = plugin.ReadQueryResponse(response);
            await OnQueryResponseParse(parseContext);

            plugin.QueryResponseContent = parseContext?.Content;

            if (plugin.IsValidQueryResponse())
            {
                var result = plugin.ParseQueryResult();

                switch (result)
                {
                    case QueryStatus.Succeeded:
                        await manager.HandleSuccessfulQueryResponse();

                        return BuildQueryResponseContext(TransactionStatus.Success, NoticeType.PaymentProvider, Constants.Messages.PaymentSuccess);
                    case QueryStatus.Failed:
                        var error = plugin.ParseFailedQueryMessage(response);

                        await manager.HandleFailedQueryResponse(error);

                        return BuildQueryResponseContext(TransactionStatus.Fail, NoticeType.PaymentProvider, error);
                    default:
                        return BuildQueryResponseContext(TransactionStatus.PendingPayment, NoticeType.PaymentProvider
                            , string.Format(Constants.MessageTemplates.PaymentPending, plugin.QueryResponseContent.PaymentPendingReason));
                }
            }
            else
            {
                throw new RequestException(
                    string.Format(Constants.MessageTemplates.PluginSignFailure, manager.PaymentChannel.AssemblyName, manager.PaymentChannel.HandlerType),
                    NoticeType.PaymentCenter
                    );
            }
        }

        private async Task OnQueryRequestSending(PaymentRequestContext context)
            => await manager.OnQueryRequestSending(context);

        private async Task OnQueryResponseReceived(string response)
        {
            payment.Succeeded = true;
            payment.Response = response;
            payment.ResponseReceivedAt = DateTime.Now.ToE8();
            await new HttpPaymentService().Update(payment);

            await manager.OnQueryResponseReceived(response);
        }

        private async Task OnQueryResponseParse(QueryResponseContext context)
            => await manager.OnQueryResponseParse(context);

        private async Task OnQueryException(Exception ex)
        {
            payment.Succeeded = false;
            payment.Response = ex.Message;

            await new HttpPaymentService().Update(payment);
        }

        internal virtual MerchantDepositQueryResponseContext BuildQueryResponseContext(TransactionStatus status, NoticeType noticeType, string content)
        {
            var context = new MerchantDepositQueryResponseContext
            {
                RequestStatus = RequestStatus.Success.ToInt32String(),
                NoticeType = noticeType.ToInt32String(),
                Status = status.ToInt32String(),
                Message = content,
                ActualAmount = plugin.QueryResponseContent.Amount,
                MerchantOrderNumber = manager.RequestedMerchantOrderNumber,
                PaymentCenterOrderNumber = manager.MerchantOrderNumber,
                PaymentOrderNumber = manager.PaymentOrderNumber
            };
            context.Sign = VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(context, string.Empty, string.Empty), manager.PaymentChannel.Merchant.MerchantKey);

            return context;
        }
        #endregion
    }
}
