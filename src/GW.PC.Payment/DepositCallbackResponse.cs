﻿using GW.PC.Models.Infrastructure;
using System;

namespace GW.PC.Payment
{
    public class DepositCallbackResponse : PaymentCallbackResponse
    {
        public Deposit Deposit { get; set; }
    }
}
