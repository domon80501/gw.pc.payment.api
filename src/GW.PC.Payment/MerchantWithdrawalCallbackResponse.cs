﻿using GW.PC.Models;

namespace GW.PC.Payment
{
    public class MerchantWithdrawalCallbackResponse : PaymentCallbackResponse
    {
        public MerchantWithdrawal MerchantWithdrawal { get; set; }
    }
}
