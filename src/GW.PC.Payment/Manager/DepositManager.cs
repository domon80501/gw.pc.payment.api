﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using GW.PC.Payment.SDK;
using GW.PC.Models.Infrastructure;
using GW.PC.Core;
using GW.PC.Services.EF.Payment;
using GW.PC.Models;
using GW.PC.Services.EF;
using GW.PC.Web.Core;
using System.Web;
using GW.PC.Merchant.Gateway.Context;
using GW.PC.Payment.Context;
using GW.PC.Services.Queues.EF;
using GW.PC.Models.Queues;
using System.Linq;

namespace GW.PC.Payment.Manager
{
    public class DepositManager : HttpPaymentManager
    {
        private Models.Merchant merchant = null;
        private Deposit deposit = null;
        private MerchantRequestContext requestContext = null;

        private bool VerifyMerchantSign(string merchantKey)
        {
            if (requestContext is MerchantDepositRequestContext depositContext)
            {
                return depositContext.Sign == VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(depositContext, string.Empty, string.Empty), merchantKey);
            }

            var queryContext = (MerchantQueryRequestContext)requestContext;

            return queryContext.Sign == VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(queryContext, string.Empty, string.Empty), merchantKey);
        }

        private async Task<Deposit> CreateDeposit()
        {
            var requestContext = (MerchantDepositRequestContext)this.requestContext;

            PaymentProviderBank ppb = null;
            IEnumerable<PaymentProviderBank> ppbConvertList = null;

            var rawText = JsonConvert.SerializeObject(requestContext);
            var requestedAmount = Convert.ToDecimal(requestContext.RequestedAmount);
            var paymentPlatform = (PaymentPlatform)Enum.Parse(typeof(PaymentPlatform), requestContext.PaymentPlatform);
            var paymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), requestContext.PaymentMethod);
            var channel = await new PaymentChannelService().SelectDepositChannel(
                merchant.Id,
                paymentMethod,
                paymentPlatform,
                requestedAmount,
                requestContext.BankCode);
            if (channel == null)
            {
                throw new RequestException(Constants.Messages.NoDepositChannel, NoticeType.PaymentCenter);
            }

            switch (paymentMethod)
            {
                case PaymentMethod.WeChat:

                    var wcDeposit = new WeChatDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        Username = requestContext.Username,
                        PaymentChannelId = channel.Id,
                        DepositIP = requestContext.Ip,
                        RequestRawText = rawText.GetSubstring(500).GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new WeChatDepositService().Add(wcDeposit);
                case PaymentMethod.Alipay:
                    var alipayDeposit = new AlipayDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        Username = requestContext.Username,
                        PaymentChannelId = channel.Id,
                        DepositIP = requestContext.Ip,
                        RequestRawText = rawText.GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new AlipayDepositService().Add(alipayDeposit);
                case PaymentMethod.OnlinePayment:
                    ppb = await new PaymentProviderBankService().GetByBankCode(
                       requestContext.BankCode,
                       channel.ProviderId,
                       PaymentType.Deposit,
                       paymentMethod);

                    if (ppb == null)
                    {
                        throw new RequestException(Constants.Messages.PaymentProviderBanksNotFound, NoticeType.PaymentCenter);
                    }

                    ProviderBankCode = ppb.Code;
                    ProviderBankName = ppb.Name;

                    var opDeposit = new OnlinePaymentDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        Username = requestContext.Username,
                        PaymentProviderBankId = ppb.Id,
                        PaymentChannelId = channel.Id,
                        DepositIP = requestContext.Ip,
                        RequestRawText = rawText.GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new OnlinePaymentDepositService().Add(opDeposit);
                case PaymentMethod.OnlineBanking:
                    ppb = await new PaymentProviderBankService().GetByBankCode(
                        requestContext.BankCode,
                        channel.ProviderId,
                        PaymentType.Deposit,
                        paymentMethod);

                    if (ppb == null)
                    {
                        throw new RequestException(Constants.Messages.PaymentProviderBanksNotFound, NoticeType.PaymentCenter);
                    }

                    ProviderBankCode = ppb.Code;
                    ProviderBankName = ppb.Name;

                    ppbConvertList = await new PaymentProviderBankService().Get(channel.ProviderId, PaymentType.Deposit, paymentMethod);

                    BankList = ppbConvertList.Select(x => new SDKBanks
                    {
                        ProviderBankCode = x.Code,
                        PaymentCenterBankCode = x.Bank.Code
                    });

                    var obDeposit = new OnlineBankingDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        Username = requestContext.Username,
                        PaymentProviderBankId = ppb.Id,
                        PaymentChannelId = channel.Id,
                        DepositIP = requestContext.Ip,
                        RequestRawText = rawText.GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new OnlineBankingDepositService().Add(obDeposit);
                case PaymentMethod.PrepaidCard:
                    ppb = await new PaymentProviderBankService().Get(
                        requestContext.BankCode,
                        channel.ProviderId,
                        PaymentType.Deposit,
                        paymentMethod);

                    if (ppb == null)
                    {
                        throw new RequestException(Constants.Messages.PaymentProviderCardsNotFound, NoticeType.PaymentCenter);
                    }

                    ProviderBankCode = ppb.Code;
                    ProviderBankName = ppb.Name;

                    var deposit = new PrepaidCardDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        Username = requestContext.Username,
                        PaymentChannelId = channel.Id,
                        DepositIP = requestContext.Ip,
                        CardSerialNumber = requestContext.CardSerialNumber,
                        CardPassword = requestContext.CardPassword,
                        PaymentProviderBankId = ppb.Id,
                        RequestRawText = rawText.GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new PrepaidCardDepositService().Add(deposit);
                case PaymentMethod.QQWallet:
                    var qqDeposit = new QQWalletDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        DepositIP = requestContext.Ip,
                        PaymentChannelId = channel.Id,
                        Username = requestContext.Username,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        RequestRawText = rawText.GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new QQWalletDepositService().Add(qqDeposit);
                case PaymentMethod.QuickPay:
                    var qpDeposit = new QuickPayDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        DepositIP = requestContext.Ip,
                        PaymentChannelId = channel.Id,
                        Username = requestContext.Username,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        RequestRawText = rawText.GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new QuickPayDepositService().Add(qpDeposit);
                case PaymentMethod.OnlineToBankCard:
                    ppbConvertList = await new PaymentProviderBankService().Get(channel.ProviderId, PaymentType.Deposit, paymentMethod);

                    if (ppbConvertList == null)
                    {
                        throw new RequestException(Constants.Messages.PaymentProviderBanksNotFound, NoticeType.PaymentCenter);
                    }

                    BankList = ppbConvertList.Select(x => new SDKBanks
                    {
                        ProviderBankCode = x.Code,
                        PaymentCenterBankCode = x.Bank.Code
                    });

                    var otbcDeposit = new OnlineToBankCardDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        Username = requestContext.Username,
                        PaymentChannelId = channel.Id,
                        DepositIP = requestContext.Ip,
                        RequestRawText = rawText.GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        PayerName = requestContext.PayerName,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new OnlineToBankCardDepositService().Add(otbcDeposit);
                case PaymentMethod.JDWallet:
                    var jdWalletDeposit = new JDWalletDeposit
                    {
                        SystemOrderNumber = await SystemOrderNumberService.Next(),
                        MerchantOrderNumber = requestContext.MerchantOrderNumber,
                        PaymentPlatform = paymentPlatform,
                        RequestedAmount = requestedAmount,
                        Username = requestContext.Username,
                        PaymentChannelId = channel.Id,
                        DepositIP = requestContext.Ip,
                        RequestRawText = rawText.GetSubstring(500),
                        MerchantId = merchant.Id,
                        RedirectionUrl = requestContext.RedirectionUrl,
                        CallbackUrl = requestContext.CallbackUrl,
                    };

                    return await new JDWalletDepositService().Add(jdWalletDeposit);
                default:
                    break;
            }

            return null;
        }

        public async Task<DepositManager> Init(MerchantRequestContext requestContext)
        {
            this.requestContext = requestContext;

            merchant = await new MerchantService().GetByUsername(requestContext.MerchantUsername);

            if (!VerifyMerchantSign(merchant.MerchantKey))
            {
                throw new RequestException(Constants.Messages.InvalidSign, NoticeType.PaymentCenter);
            }

            if (requestContext is MerchantDepositRequestContext)
            {
                deposit = await DepositServiceHelper.GetDeposit(
                    merchant.Id
                    , (PaymentMethod)Enum.Parse(typeof(PaymentMethod), ((MerchantDepositRequestContext)requestContext).PaymentMethod)
                    , requestContext.MerchantOrderNumber);

                if (deposit != null)
                {
                    throw new RequestException(Constants.Messages.OrderNumberDuplicated, NoticeType.PaymentCenter);
                }

                deposit = await CreateDeposit();
            }
            else
            {
                deposit = await DepositServiceHelper.GetDeposit(
                    merchant.Id
                    , (PaymentMethod)Enum.Parse(typeof(PaymentMethod), ((MerchantQueryRequestContext)requestContext).PaymentMethod)
                    , requestContext.MerchantOrderNumber
                    , "PaymentChannel.Provider");

                if (deposit == null)
                {
                    throw new RequestException(Constants.Messages.InvalidMerchantOrderNumber, NoticeType.PaymentCenter);
                }
            }

            Common.CheckNull(deposit, nameof(deposit));

            deposit.PaymentChannel.Merchant = merchant;

            handler = await HttpPaymentHandlerFactory.CreateHandler(deposit.PaymentChannel.AssemblyName, deposit.PaymentChannel.HandlerType, this);

            return this;
        }

        public override async Task<HttpPaymentManager> Init(int paymentChannelId)
        {
            var config = await new PaymentChannelService().GetHandlerConfig(paymentChannelId);

            handler = await HttpPaymentHandlerFactory.CreateHandler(config.Item1, config.Item2, this);

            return this;
        }

        #region Set parameters   

        internal override string RequestedMerchantOrderNumber => deposit.MerchantOrderNumber;
        internal override string PaymentOrderNumber => deposit.PaymentOrderNumber;
        internal override PaymentChannel PaymentChannel => deposit.PaymentChannel;
        #region Set payment channel's parameters
        public override string MerchantUsername => PaymentChannel.MerchantUsername;
        public override string MerchantKey => PaymentChannel.MerchantKey;
        public override PayType PayType => (PayType)PaymentChannel.PaymentType;
        public override PayMethod PayMethod => (PayMethod)PaymentChannel.PaymentMethod;
        public override PayPlatform PayPlatform => (PayPlatform)PaymentChannel.PaymentPlatform;
        public override string RequestUrl => PaymentChannel.RequestUrl;
        public override string CallbackUrl => PaymentChannel.CallbackUrl;
        public override string QueryUrl => PaymentChannel.QueryUrl;

        public override string Arguments => PaymentChannel.Arguments;

        #endregion

        public override string Username => deposit.Username;

        public override decimal RequestedAmount => deposit.RequestedAmount;

        public override string MerchantOrderNumber => deposit.PaymentCenterOrderNumber;

        public override string RequestRawText => deposit.RequestRawText;

        public override string PaymentIP => deposit.DepositIP;

        private string ProviderBankCode { get; set; }
        public override string BankCode => ProviderBankCode;

        private string ProviderBankName { get; set; }
        public override string BankName => ProviderBankName;

        internal override HttpPayment BuildHttpPayment() => deposit?.BuildHttpPayment();

        internal override int? PaymentProviderBankId
        {
            get
            {
                if (deposit is OnlineBankingDeposit obd)
                {
                    return obd.PaymentProviderBankId;
                }
                //else if (deposit is InternalDeposit id)
                //{
                //    return id.PaymentProviderBankId;
                //}
                else
                {
                    return null;
                }
            }
        }

        public override string PayerName
        {
            get
            {
                if (deposit is OnlineToBankCardDeposit otbcd)
                {
                    return otbcd.PayerName;
                }
                else
                {
                    return null;
                }
            }
        }

        public override string RedirectionUrl => deposit.RedirectionUrl;

        #endregion

        #region Payment
        protected internal override async Task OnPaymentException(Exception ex)
        {
            await Fail(ex.Message);

            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_Failed,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = ex.ToString().GetSubstring(500)
            });
        }

        internal override async Task OnPaymentRequestSending(PaymentRequestContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_PaymentRequestSending,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = JsonConvert.SerializeObject(context).GetSubstring(500)
            });
        }

        internal override async Task OnPaymentHttpRequestSending(PaymentGatewayRequestContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_PaymentHttpRequestSending,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = JsonConvert.SerializeObject(context).GetSubstring(500)
            });
        }

        internal override async Task OnPaymentResponseReceived(string response)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_PaymentResponseReceived,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = response.GetSubstring(500)
            });
        }

        internal override async Task OnPaymentResponseParse(PaymentResponseContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_PaymentResponseParse,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = JsonConvert.SerializeObject(context).GetSubstring(500)
            });
        }

        protected internal override async Task HandleSuccessfulPaymentResponse()
        {
            if (handler.plugin.PaymentResponseContent is OnlineBankingDepositPaymentResponseContext content)
            {
                if (deposit is OnlineBankingDeposit obd)
                {
                    obd.PayeeAccountNumber = !string.IsNullOrEmpty(content.CardNumber) ?
                        content.CardNumber : content.Email;
                    obd.PayeeName = content.PayeeName;
                    obd.Remark = content.Remark;
                    obd.PaymentOrderNumber = content.PaymentOrderNumber;

                    deposit = await DepositServiceHelper.Update(obd);
                }
                else if (deposit is OnlineToBankCardDeposit otbcd)
                {
                    otbcd.PayeeCardNumber = !string.IsNullOrEmpty(content.CardNumber) ?
                        content.CardNumber : content.Email;
                    otbcd.PayeeName = content.PayeeName;
                    otbcd.Remark = content.Remark;
                    otbcd.PaymentOrderNumber = content.PaymentOrderNumber;

                    deposit = await DepositServiceHelper.Update(otbcd);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(handler.plugin.PaymentResponseContent?.PaymentOrderNumber))
                {
                    deposit.PaymentOrderNumber = handler.plugin.PaymentResponseContent.PaymentOrderNumber;
                    deposit = await DepositServiceHelper.Update(deposit);
                }                
            }

            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_SuccessfulPaymentResponse,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id
            });

            var queueContext = new PaymentQueryRequest
            {
                RequestId = Guid.NewGuid().ToString(),
                MerchantUsername = merchant.Username,
                PaymentType = PaymentChannel.PaymentType,
                PaymentMethod = PaymentChannel.PaymentMethod,
                MerchantOrderNumber = deposit.MerchantOrderNumber,
                CreatedAt = DateTime.Now.ToE8(),
                NextSendingAt = DateTime.Now.ToE8(),
            };

            await new PaymentQueryRequestService().Add(queueContext);
        }

        protected internal override async Task HandleFailedPaymentResponse(string response)
        {
            await Fail(response);

            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_Failed,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = response.GetSubstring(500)
            });
        }
        #endregion

        #region Query
        protected internal override async Task HandleFailedQueryResponse(string error)
        {
            if (deposit.Status == DepositStatus.PendingPayment)
            {
                await Fail(error);
            }

            await AddMerchantCallbackQueue();
        }

        protected internal override async Task HandleSuccessfulQueryResponse()
        {
            if (deposit.Status == DepositStatus.PendingPayment)
            {
                await Finalize(true, Convert.ToDecimal(handler.plugin.QueryResponseContent.Amount));
            }

            await AddMerchantCallbackQueue();
        }

        internal override async Task OnQueryRequestSending(PaymentRequestContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_QueryRequestSending,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = JsonConvert.SerializeObject(context).GetSubstring(500)
            });
        }

        internal override async Task OnQueryResponseReceived(string response)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_QueryResponseReceived,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = response.GetSubstring(500)
            });
        }

        internal override async Task OnQueryResponseParse(QueryResponseContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_QueryResponseParse,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = JsonConvert.SerializeObject(context).GetSubstring(500)
            });
        }

        #endregion

        #region Callback
        public override async Task<object> ParseCallback(PaymentCallbackRequestContext requestContext)
        {
            try
            {
                var parseContext = handler.plugin.ReadCallbackRequest(requestContext.Content);

                handler.plugin.CallbackRequestContent = parseContext.Content;

                var paymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), requestContext.PaymentMethod.ToString());

                deposit = await DepositServiceHelper.GetDeposit(paymentMethod, handler.plugin.ExtractCallbackMerchantOrderNumber(), "PaymentChannel.Provider", "Merchant");
                if (deposit == null)
                {
                    return SkipCallback(Constants.Messages.InvalidMerchantOrderNumber);
                }

                await OnCallbackRequestReceived(requestContext.Content);
                await OnCallbackRequestParse(parseContext);

                if (!handler.plugin.IsValidCallback(deposit.PaymentChannel.MerchantKey))
                {
                    throw new Exception(Constants.Messages.InvalidSign);
                }

                switch (deposit.Status)
                {
                    case DepositStatus.PendingPayment:
                    case DepositStatus.AwaitReminderApproval:
                        break;
                    case DepositStatus.AutoSuccess:
                    case DepositStatus.ManualSuccess:
                    case DepositStatus.PaymentFailed:
                        return SkipCallback();
                    default:
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                deposit.Status));
                }

                deposit.CallbackRawText = requestContext.Content.GetSubstring(500);

                if (deposit is OnlineBankingDeposit obd)
                {
                    obd.PayerName = handler.plugin.CallbackRequestContent.PayerName;
                }
                else if (deposit is OnlineToBankCardDeposit otbcd)
                {
                    otbcd.PayerName = handler.plugin.CallbackRequestContent.PayerName;
                }

                return deposit;
            }
            catch (Exception ex)
            {
                await OnCallbackResponseSending(ex.ToString());

                return FailCallback(ex);
            }
        }

        public override async Task<PaymentCallbackResponse> Callback(PaymentCallbackRequestContext request)
        {
            try
            {
                await Finalize(handler.plugin.IsSuccessfulCallback(), handler.GetCallbackAmount());

                await AddMerchantCallbackQueue();

                return await SucceedCallback();
            }
            catch (Exception ex)
            {
                await OnCallbackResponseSending(ex.ToString());

                return FailCallback(ex);
            }
            finally
            {
                AppDomain.Unload(handler.appDomain);
            }
        }

        public override async Task AddMerchantCallbackQueue()
        {
            var queue = new MerchantCallbackRequest
            {
                RequestId = Guid.NewGuid().ToString(),
                MerchantCallbackUrl = deposit.CallbackUrl,
                MerchantUsername = PaymentChannel.Merchant.Username,
                MerchantOrderNumber = deposit.MerchantOrderNumber,
                PaymentCenterOrderNumber = deposit.PaymentCenterOrderNumber,
                PaymentOrderNumber = deposit.PaymentOrderNumber,
                PaymentType = PaymentChannel.PaymentType,
                PaymentMethod = PaymentChannel.PaymentMethod,
                RequestedAmount = deposit.RequestedAmount.ToString(),
                ActualAmount = deposit.ActualAmount.ToString(),
                SendCount = 0,
                CreatedAt = DateTime.Now.ToE8(),
                NextSendingAt = DateTime.Now.ToE8(),
            };

            switch (deposit.Status)
            {
                case DepositStatus.AutoSuccess:
                case DepositStatus.ManualSuccess:
                case DepositStatus.Success:
                    queue.Status = TransactionStatus.Success;
                    break;
                case DepositStatus.PaymentFailed:
                    queue.Status = TransactionStatus.Fail;
                    break;
                default:
                    queue.Status = TransactionStatus.UnKnow;
                    break;
            }

            await new MerchantCallbackRequestService().Add(queue);
        }

        internal override async Task OnCallbackRequestParse(CallbackRequestContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_CallbackRequestParse,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = JsonConvert.SerializeObject(context)
            });
        }

        internal override async Task OnCallbackRequestReceived(string callback)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_CallbackRequestReceived,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit.EntityLogTargetType,
                TargetId = deposit.Id,
                Details = callback
            });
        }

        internal override async Task OnCallbackResponseSending(string response)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.Deposit_CallbackResponseSending,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = deposit?.EntityLogTargetType ?? EntityLogTargetType.Unknown,
                TargetId = deposit?.Id,
                Details = response.Substring(0, (response.Length > 2000 ? 2000 : response.Length))
            });
        }

        protected override PaymentCallbackResponse SkipCallback(string reason = "")
        {
            var response = new PaymentCallbackResponse
            {
                PaymentCallbackResult = PaymentCallbackResult.Skipped,
                Content = string.IsNullOrEmpty(reason) ? handler.plugin.CallbackSuccess() : reason
            };

            return response;
        }

        protected override async Task<PaymentCallbackResponse> SucceedCallback()
        {
            var response = new DepositCallbackResponse
            {
                Deposit = deposit,
                Content = handler.plugin.CallbackSuccess(),
                PaymentCallbackResult = PaymentCallbackResult.Successful
            };

            await OnCallbackResponseSending(response.Content);

            return response;
        }

        protected override PaymentCallbackResponse FailCallback(Exception exception)
        {
            var response = new DepositCallbackResponse
            {
                Deposit = deposit,
                Content = exception.Message,
                Exception = exception
            };

            return response;
        }
        #endregion

        internal override async Task Finalize(bool result, decimal amount)
        {
            switch (deposit.PaymentMethod)
            {
                case PaymentMethod.WeChat:
                    await new WeChatDepositService().Callback((WeChatDeposit)deposit, result, amount);

                    break;
                case PaymentMethod.Alipay:
                    await new AlipayDepositService().Callback((AlipayDeposit)deposit, result, amount);

                    break;
                case PaymentMethod.OnlinePayment:
                    await new OnlinePaymentDepositService().Callback((OnlinePaymentDeposit)deposit, result, amount);

                    break;
                case PaymentMethod.OnlineBanking:
                    await new OnlineBankingDepositService().Callback((OnlineBankingDeposit)deposit, result, amount);

                    break;
                case PaymentMethod.PrepaidCard:
                    await new PrepaidCardDepositService().Callback((PrepaidCardDeposit)deposit, result, amount);

                    break;
                case PaymentMethod.QQWallet:
                    await new QQWalletDepositService().Callback((QQWalletDeposit)deposit, result, amount);

                    break;
                case PaymentMethod.QuickPay:
                    await new QuickPayDepositService().Callback((QuickPayDeposit)deposit, result, amount);

                    break;
                case PaymentMethod.OnlineToBankCard:
                    await new OnlineToBankCardDepositService().Callback((OnlineToBankCardDeposit)deposit, result, amount);

                    break;
                case PaymentMethod.JDWallet:
                    await new JDWalletDepositService().Callback((JDWalletDeposit)deposit, result, amount);

                    break;
                default:
                    break;
            }
        }

        private async Task<object> Fail(string message)
        {
            switch (deposit.PaymentMethod)
            {
                case PaymentMethod.WeChat:
                    await new WeChatDepositService().Fail((WeChatDeposit)deposit);

                    return deposit;
                case PaymentMethod.Alipay:
                    await new AlipayDepositService().Fail((AlipayDeposit)deposit);

                    return deposit;
                case PaymentMethod.QQWallet:
                    await new QQWalletDepositService().Fail((QQWalletDeposit)deposit);

                    return deposit;
                case PaymentMethod.QuickPay:
                    await new QuickPayDepositService().Fail((QuickPayDeposit)deposit);

                    return deposit;
                case PaymentMethod.OnlineBanking:
                    await new OnlineBankingDepositService().Fail((OnlineBankingDeposit)deposit);

                    return deposit;
                case PaymentMethod.OnlineToBankCard:
                    await new OnlineToBankCardDepositService().Fail((OnlineToBankCardDeposit)deposit);

                    return deposit;
                case PaymentMethod.PrepaidCard:
                    await new PrepaidCardDepositService().Fail((PrepaidCardDeposit)deposit);

                    return deposit;
                case PaymentMethod.OnlinePayment:
                    await new OnlinePaymentDepositService().Fail((OnlinePaymentDeposit)deposit);

                    return deposit;
                case PaymentMethod.JDWallet:
                    await new JDWalletDepositService().Fail((JDWalletDeposit)deposit);

                    return deposit;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}
