﻿using GW.PC.Core;
using GW.PC.Merchant.Gateway.Context;
using GW.PC.Models;
using GW.PC.Payment.Context;
using GW.PC.Payment.SDK;
using GW.PC.Web.Core;
using System;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;

namespace GW.PC.Payment.Manager
{
    public abstract class HttpPaymentManager : PaymentManagerSDK
    {
        protected HttpPaymentHandler handler = null;

        internal virtual int? PaymentProviderBankId { get; }
        internal virtual int? PayeeBankId { get; }
        internal virtual string PaymentOrderNumber { get; }

        internal abstract string RequestedMerchantOrderNumber { get; }
        internal abstract HttpPayment BuildHttpPayment();
        internal abstract PaymentChannel PaymentChannel { get; }

        public async Task<HttpPaymentManager> Init(string assemblyFullName, string handlerFullName)
        {
            handler = await HttpPaymentHandlerFactory.CreateHandler(assemblyFullName, handlerFullName, this);

            return this;
        }

        /// <summary>
        /// For callback using.
        /// </summary>
        /// <param name="paymentChannelId"></param>        
        /// <returns></returns>
        public abstract Task<HttpPaymentManager> Init(int paymentChannelId);

        #region Payment
        public virtual async Task<MerchantResponseContext> Request()
        {
            try
            {
                Common.CheckNull(handler, nameof(handler));
                Common.CheckNull(PaymentChannel, nameof(PaymentChannel));

                await EnsureMerchantOrderNumber();

                return await handler.Request();
            }
            catch (Exception ex)
            {
                await OnPaymentException(ex);

                if (this is WithdrawalManager)
                {
                    return handler.BuildResponseContext(TransactionStatus.UnKnow, NoticeType.PaymentCenter
                        , string.Format(Constants.MessageTemplates.PaymentRequestFailure, ex.Message));
                }

                throw ex;
            }
            finally
            {
                AppDomain.Unload(handler.appDomain);
            }
        }

        protected virtual Task EnsureMerchantOrderNumber() => Task.CompletedTask;

        internal virtual Task OnPaymentRequestSending(PaymentRequestContext context) => Task.CompletedTask;

        internal virtual Task OnPaymentHttpRequestSending(PaymentGatewayRequestContext context) => Task.CompletedTask;

        internal virtual Task OnPaymentResponseReceived(string response) => Task.CompletedTask;

        internal virtual Task OnPaymentResponseParse(PaymentResponseContext context) => Task.CompletedTask;

        protected internal abstract Task OnPaymentException(Exception ex);

        protected internal virtual Task HandleSuccessfulPaymentResponse() => Task.CompletedTask;

        protected internal virtual Task HandleFailedPaymentResponse(string response) => Task.CompletedTask;
        #endregion

        #region Query
        public virtual async Task<MerchantQueryResponseContext> Query()
        {
            Common.CheckNull(handler, nameof(handler));
            Common.CheckNull(PaymentChannel, nameof(PaymentChannel));

            return await handler.Query();
        }

        internal virtual Task OnQueryRequestSending(PaymentRequestContext context) => Task.CompletedTask;

        internal virtual Task OnQueryResponseReceived(string response) => Task.CompletedTask;

        internal virtual Task OnQueryResponseParse(QueryResponseContext context) => Task.CompletedTask;

        protected internal virtual Task HandleFailedQueryResponse(string response) => Task.CompletedTask;

        protected internal virtual Task OnQueryException(Exception ex) => Task.CompletedTask;
        #endregion

        #region Callback
        public virtual Task<object> ParseCallback(PaymentCallbackRequestContext requestContext) => Task.FromResult<object>(null);

        public abstract Task<PaymentCallbackResponse> Callback(PaymentCallbackRequestContext request);

        public abstract Task AddMerchantCallbackQueue();

        internal virtual Task OnCallbackRequestParse(CallbackRequestContext context) => Task.CompletedTask;

        internal virtual Task OnCallbackRequestReceived(string callback) => Task.CompletedTask;

        internal virtual Task OnCallbackResponseSending(string response) => Task.CompletedTask;

        protected internal virtual Task HandleSuccessfulQueryResponse() => Task.CompletedTask;

        protected virtual PaymentCallbackResponse SkipCallback(string reason = "") =>
            new PaymentCallbackResponse
            {
                PaymentCallbackResult = PaymentCallbackResult.Skipped
            };

        protected abstract Task<PaymentCallbackResponse> SucceedCallback();

        protected abstract PaymentCallbackResponse FailCallback(Exception exception);
        #endregion

        internal virtual Task Finalize(bool result, decimal amount) => Task.CompletedTask;

        private HttpPaymentManager Init(HttpPaymentHandler handler)
        {
            this.handler = handler;

            return this;
        }
    }
}
