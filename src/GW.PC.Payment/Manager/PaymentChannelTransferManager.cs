﻿using AP.Core;
using AP.Models;
using AP.Services.EF;
using AP.Web.Core;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace AP.Payment.Manager
{
    public class PaymentChannelTransferManager : HttpPaymentManager
    {
        private PaymentChannelTransfer transfer = null;

        public async Task<PaymentChannelTransferManager> Init(PaymentChannelTransfer transfer)
        {
            Common.CheckNull(transfer?.From, nameof(transfer.From));

            this.transfer = transfer;

            handler = await HttpPaymentHandlerFactory.CreateHandler(transfer.From.HandlerType, this);

            return this;
        }

        internal override HttpPayment BuildHttpPayment() => transfer?.BuildHttpPayment();

        internal override string MerchantOrderNumber => transfer.MerchantOrderNumber;

        internal override string PayeeCardNumber => transfer.PayeeCardNumber;

        internal override string PayeeName => transfer.PayeeName;

        internal override PaymentChannel PaymentChannel => transfer.From;

        internal override decimal RequestedAmount => transfer.Amount;

        internal override int? PayeeBankId => transfer.PayeeBankId;

        #region Payment
        internal override async Task OnPaymentRequestSending(PaymentRequest request)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelTransfer_PaymentRequestSending,
                EntityLogTargetType.PaymentChannelTransfer,
                transfer.Id,
                JsonConvert.SerializeObject(request),
                transfer.Amount);
        }

        internal override async Task OnPaymentResponseReceived(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelTransfer_PaymentResponseReceived,
                EntityLogTargetType.PaymentChannelTransfer,
                transfer.Id,
                response,
                transfer.Amount);
        }

        protected internal override async Task OnPaymentException(Exception ex)
        {
            transfer = await Fail(ex.Message, logDetails: ex.ToString());
        }

        protected internal override async Task HandleSuccessfulPaymentResponse()
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelTransfer_SuccessfulPaymentResponse,
                EntityLogTargetType.PaymentChannelTransfer,
                transfer.Id,
                null,
                transfer.Amount);
        }

        protected internal override async Task HandleFailedPaymentResponse(string response)
        {
            transfer = await Fail(response, logDetails: response);
        }
        #endregion

        #region Callback
        public override async Task<PaymentCallbackResponse> Callback(PaymentCallbackRequestContext request)
        {
            try
            {
                var service = new PaymentChannelTransferService();
                transfer = await service.GetByMerchantOrderNumber(handler.ExtractCallbackMerchantOrderNumber(), "From");
                if (transfer == null)
                {
                    throw new BusinessException(Constants.Messages.InvalidMerchantOrderNumber);
                }

                await OnCallbackRequestReceived(request.Content);

                if (!handler.IsValidCallback(transfer.From.MerchantKey))
                {
                    throw new BusinessException(Constants.Messages.InvalidSign);
                }

                switch (transfer.Status)
                {
                    case PaymentChannelTransferStatus.AutoPaymentFailed:
                    case PaymentChannelTransferStatus.AutoPaymentManualConfirmedFailed:
                    case PaymentChannelTransferStatus.AutoPaymentSuccessful:
                    case PaymentChannelTransferStatus.ManualConfirmed:
                        return await SucceedCallback();
                    case PaymentChannelTransferStatus.AutoPaymentInProgress:
                        break;
                    default:
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                transfer.Status));
                }

                if (handler.IsSuccessfulCallback())
                {
                    transfer = await service.AutoSucceed(transfer.Id);
                }
                else
                {
                    transfer = await service.AutoFail(transfer.Id, handler.GetCallbackErrorMessage(request.Content));
                }

                return await SucceedCallback();
            }
            catch (Exception ex)
            {
                await OnCallbackResponseSending(ex.ToString());

                return FailCallback(ex);
            }
        }

        internal override async Task OnCallbackRequestReceived(string callback)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelTransfer_CallbackRequestReceived,
                EntityLogTargetType.PaymentChannelTransfer,
                transfer.Id,
                callback,
                transfer.Amount);
        }

        internal override async Task OnCallbackResponseSending(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelTransfer_CallbackResponseSending,
                EntityLogTargetType.PaymentChannelTransfer,
                transfer?.Id,
                response,
                transfer?.Amount);
        }

        protected override async Task<PaymentCallbackResponse> SucceedCallback()
        {
            var response = new PaymentChannelTransferCallbackResponse
            {
                PaymentChannelTransfer = transfer,
                Content = handler.CallbackSuccess()
            };

            await OnCallbackResponseSending(response.Content);

            return response;
        }

        protected override PaymentCallbackResponse FailCallback(Exception exception)
        {
            var response = new PaymentChannelTransferCallbackResponse
            {
                PaymentChannelTransfer = transfer,
                Content = exception.Message
            };

            return response;
        }
        #endregion

        #region Query
        internal override async Task OnQueryRequestSending(PaymentRequest request)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelTransfer_QueryRequestSending,
                EntityLogTargetType.PaymentChannelTransfer,
                transfer.Id,
                JsonConvert.SerializeObject(request),
                transfer.Amount);
        }

        internal override async Task OnQueryResponseReceived(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelTransfer_QueryResponseReceived,
                EntityLogTargetType.PaymentChannelTransfer,
                transfer.Id,
                response,
                transfer.Amount);
        }

        protected internal override Task HandleSuccessfulQueryResponse(PaymentQueryRequestContext requestContext)
        {
            return new PaymentChannelTransferService().AutoSucceed(transfer.Id);
        }

        protected internal override Task HandleFailedQueryResponse(string declineNotes)
        {
            return new PaymentChannelTransferService().AutoFail(transfer.Id, declineNotes);
        }
        #endregion

        protected override async Task EnsureMerchantOrderNumber()
        {
            if (string.IsNullOrEmpty(transfer.MerchantOrderNumber))
            {
                transfer.MerchantOrderNumber = $"{Constants.MerchantOrderNumberPrefixes.PaymentChannelTransfer}{handler.EnsureMerchantOrderNumber()}";

                await new PaymentChannelTransferService().Update(transfer);
            }
        }

        private async Task<PaymentChannelTransfer> Fail(string message, string logDetails = null)
        {
            return await new PaymentChannelTransferService().AutoFail(transfer.Id, message, logDetails);
        }
    }
}
