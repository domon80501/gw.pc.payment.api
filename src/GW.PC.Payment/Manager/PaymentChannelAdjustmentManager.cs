﻿using AP.Core;
using AP.Models;
using AP.Services.EF;
using AP.Web.Core;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace AP.Payment.Manager
{
    public class PaymentChannelAdjustmentManager : HttpPaymentManager
    {
        private PaymentChannelAdjustment adjustment = null;

        public async Task<PaymentChannelAdjustmentManager> Init(PaymentChannelAdjustment adjustment)
        {
            Common.CheckNull(adjustment?.PaymentChannel, nameof(adjustment.PaymentChannel));

            this.adjustment = adjustment;

            handler = await HttpPaymentHandlerFactory.CreateHandler(adjustment.PaymentChannel.HandlerType, this);

            return this;
        }

        internal override HttpPayment BuildHttpPayment() => adjustment?.BuildHttpPayment();

        internal override string MerchantOrderNumber => adjustment.MerchantOrderNumber;

        internal override string PayeeCardNumber => adjustment.PayeeCardNumber;

        internal override string PayeeName => adjustment.PayeeName;

        internal override PaymentChannel PaymentChannel => adjustment.PaymentChannel;

        internal override decimal RequestedAmount => Math.Abs(adjustment.Amount);

        internal override int? PayeeBankId => adjustment.PayeeBankId;

        #region Payment
        internal override async Task OnPaymentRequestSending(PaymentRequest request)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelAdjustment_PaymentRequestSending,
                EntityLogTargetType.PaymentChannelAdjustment,
                adjustment.Id,
                JsonConvert.SerializeObject(request),
                adjustment.Amount);
        }

        internal override async Task OnPaymentResponseReceived(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelAdjustment_PaymentResponseReceived,
                EntityLogTargetType.PaymentChannelAdjustment,
                adjustment.Id,
                response,
                adjustment.Amount);
        }

        protected internal override async Task OnPaymentException(Exception ex)
        {
            adjustment = await Fail(ex.Message, logDetails: ex.ToString());
        }

        protected internal override async Task HandleSuccessfulPaymentResponse()
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelAdjustment_SuccessfulPaymentResponse,
                EntityLogTargetType.PaymentChannelAdjustment,
                adjustment.Id,
                null,
                adjustment.Amount);
        }

        protected internal override async Task HandleFailedPaymentResponse(string response)
        {
            adjustment = await Fail(response, logDetails: response);
        }
        #endregion

        #region Callback
        public override async Task<PaymentCallbackResponse> Callback(PaymentCallbackRequestContext request)
        {
            try
            {
                var service = new PaymentChannelAdjustmentService();
                adjustment = await service.GetByMerchantOrderNumber(handler.ExtractCallbackMerchantOrderNumber(), "PaymentChannel");
                if (adjustment == null)
                {
                    throw new BusinessException(Constants.Messages.InvalidMerchantOrderNumber);
                }

                await OnCallbackRequestReceived(request.Content);

                if (!handler.IsValidCallback(adjustment.PaymentChannel.MerchantKey))
                {
                    throw new BusinessException(Constants.Messages.InvalidSign);
                }

                switch (adjustment.Status)
                {
                    case PaymentChannelAdjustmentStatus.AutoPaymentFailed:
                    case PaymentChannelAdjustmentStatus.AutoPaymentManualConfirmedFailed:
                    case PaymentChannelAdjustmentStatus.AutoPaymentSuccessful:
                    case PaymentChannelAdjustmentStatus.ManualConfirmed:
                        return await SucceedCallback();
                    case PaymentChannelAdjustmentStatus.AutoPaymentInProgress:
                        break;
                    default:
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                adjustment.Status));
                }

                if (handler.IsSuccessfulCallback())
                {
                    adjustment = await service.AutoSucceed(adjustment.Id);
                }
                else
                {
                    adjustment = await service.AutoFail(adjustment.Id, handler.GetCallbackErrorMessage(request.Content));
                }

                return await SucceedCallback();
            }
            catch (Exception ex)
            {
                await OnCallbackResponseSending(ex.ToString());

                return FailCallback(ex);
            }
        }

        internal override async Task OnCallbackRequestReceived(string callback)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelAdjustment_CallbackRequestReceived,
                EntityLogTargetType.PaymentChannelAdjustment,
                adjustment.Id,
                callback,
                adjustment.Amount);
        }

        internal override async Task OnCallbackResponseSending(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.PaymentChannelAdjustment_CallbackResponseSending,
                EntityLogTargetType.PaymentChannelAdjustment,
                adjustment?.Id,
                response,
                adjustment?.Amount);
        }

        protected override async Task<PaymentCallbackResponse> SucceedCallback()
        {
            var response = new PaymentChannelAdjustmentCallbackResponse
            {
                PaymentChannelAdjustment = adjustment,
                Content = handler.CallbackSuccess()
            };

            await OnCallbackResponseSending(response.Content);

            return response;
        }

        protected override PaymentCallbackResponse FailCallback(Exception exception)
        {
            var response = new PaymentChannelAdjustmentCallbackResponse
            {
                PaymentChannelAdjustment = adjustment,
                Content = exception.Message
            };

            return response;
        }
        #endregion

        #region Query
        //internal override async Task OnQueryRequestSending(PaymentRequest request)
        //{
        //    await new EntityLogService().Add(new EntityLog
        //    {
        //        Content = EntityLogContent.CustomerWithdrawal_QueryRequestSending,
        //        CreatedAt = DateTime.Now.ToE8(),
        //        TargetType = EntityLogTargetType.CustomerWithdrawal,
        //        TargetId = transfer.Id,
        //        Data = EntityLogContents.SetData(transfer.Amount),
        //        Details = JsonConvert.SerializeObject(request)
        //    });
        //}

        //internal override async Task OnQueryResponseReceived(string response)
        //{
        //    await new EntityLogService().Add(new EntityLog
        //    {
        //        Content = EntityLogContent.CustomerWithdrawal_QueryResponseReceived,
        //        CreatedAt = DateTime.Now.ToE8(),
        //        TargetType = EntityLogTargetType.CustomerWithdrawal,
        //        TargetId = transfer.Id,
        //        Data = EntityLogContents.SetData(transfer.Amount),
        //        Details = response
        //    });
        //}

        //protected internal override Task HandleSuccessfulQueryResponse()
        //{
        //    return new CustomerWithdrawalService().AutoSucceed(transfer.Id);
        //}

        //protected internal override Task HandleFailedQueryResponse(string declineNotes)
        //{
        //    return new CustomerWithdrawalService().AutoFail(transfer.Id, declineNotes);
        //}
        #endregion

        protected override async Task EnsureMerchantOrderNumber()
        {
            if (string.IsNullOrEmpty(adjustment.MerchantOrderNumber))
            {
                adjustment.MerchantOrderNumber = $"{Constants.MerchantOrderNumberPrefixes.PaymentChannelAdjustment}{handler.EnsureMerchantOrderNumber()}";

                await new PaymentChannelAdjustmentService().Update(adjustment);
            }
        }

        private async Task<PaymentChannelAdjustment> Fail(string message, string logDetails = null)
        {
            return await new PaymentChannelAdjustmentService().AutoFail(adjustment.Id, message, logDetails);
        }
    }
}
