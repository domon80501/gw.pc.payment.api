﻿using AP.Core;
using AP.Models;
using AP.Services.EF;
using AP.Web.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AP.Payment.Manager
{
    public class AgentWithdrawalManager : HttpPaymentManager
    {
        private AgentWithdrawal withdrawal = null;

        public async Task<AgentWithdrawalManager> Init(AgentWithdrawal withdrawal)
        {
            Common.CheckNull(withdrawal?.PaymentChannel, nameof(withdrawal.PaymentChannel));

            this.withdrawal = withdrawal;

            handler = await HttpPaymentHandlerFactory.CreateHandler(withdrawal.PaymentChannel.HandlerType, this);

            return this;
        }

        internal override HttpPayment BuildHttpPayment() => withdrawal?.BuildHttpPayment();

        internal override string MerchantOrderNumber => withdrawal.MerchantOrderNumber;

        internal override string PayeeCardNumber => withdrawal.PayeeCardNumber;

        internal override string PayeeName => withdrawal.PayeeName;

        internal override PaymentChannel PaymentChannel => withdrawal.PaymentChannel;

        internal override decimal RequestedAmount => withdrawal.Amount;

        internal override int? PayeeBankId => withdrawal.PayeeBankId;

        #region Payment
        public override async Task<string> Request()
        {
            var divideThreshold = (await SystemConfigurationService.Get()).Withdrawal.DivideThreshold;
            if (withdrawal.Amount > divideThreshold)
            {
                var childStatus = WithdrawalStatus.AutoPaymentInProgress;
                if (withdrawal.Amount > new PaymentProviderService().GetEnabledTotalBalance())
                {
                    childStatus = WithdrawalStatus.AwaitingApproval;
                }

                var children = new List<AgentWithdrawal>();
                var count = (int)Math.Ceiling(withdrawal.Amount / divideThreshold.Value);

                for (int i = 0; i < count; i++)
                {
                    // 倒数第二笔拆分，要考虑最后一笔的最低通道额度
                    var amount = divideThreshold.Value;
                    if (i == count - 2)
                    {
                        if (withdrawal.Amount % divideThreshold < withdrawal.PaymentChannel.MinAmount)
                        {
                            amount = withdrawal.Amount - children.Sum(w => w.Amount) - withdrawal.PaymentChannel.MinAmount;
                        }
                    }
                    else if (i == count - 1)
                    {
                        amount = withdrawal.Amount - children.Sum(w => w.Amount);
                    }

                    children.Add(new AgentWithdrawal
                    {
                        Amount = amount,
                        CreatedAt = DateTime.Now.ToE8(),
                        PayeeBankId = withdrawal.PayeeBankId,
                        PayeeCardNumber = withdrawal.PayeeCardNumber,
                        PayeeCardAddress = withdrawal.PayeeCardNumber,
                        PayeeName = withdrawal.PayeeName,
                        Status = childStatus,
                        Username = withdrawal.Username,
                        WithdrawalIP = withdrawal.WithdrawalIP,
                        WithdrawalAddress = withdrawal.WithdrawalAddress,
                        PaymentChannelId = withdrawal.PaymentChannelId,
                        ParentId = withdrawal.Id
                    });
                }

                withdrawal = await new AgentWithdrawalService().AddChildren(withdrawal, children);

                await EntityLogService.Add(
                    EntityLogContent.AgentWithdrawal_DividingCompleted,
                    EntityLogTargetType.AgentWithdrawal,
                    withdrawal.Id,
                    null,
                    withdrawal.Amount, divideThreshold.Value, children.Count, string.Join(",", children.Select(w => w.Amount)));

                var withdrawals = await new AgentWithdrawalService().GetChildren(withdrawal.Id);
                foreach (var childWithdrawal in withdrawals)
                {
                    var manager = await new AgentWithdrawalManager().Init(childWithdrawal);

                    await manager.Request();
                }

                return null;
            }
            else
            {
                return await base.Request();
            }
        }

        internal override async Task OnPaymentRequestSending(PaymentRequest request)
        {
            await EntityLogService.Add(
                EntityLogContent.AgentWithdrawal_PaymentRequestSending,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal.Id,
                JsonConvert.SerializeObject(request),
                withdrawal.Amount);
        }

        internal override async Task OnPaymentResponseReceived(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.AgentWithdrawal_PaymentResponseReceived,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal.Id,
                response,
                withdrawal.Amount);
        }

        protected internal override async Task OnPaymentException(Exception ex)
        {
            withdrawal = await Fail(ex.Message, logDetails: ex.ToString());
        }

        protected internal override async Task HandleSuccessfulPaymentResponse()
        {
            await EntityLogService.Add(
                EntityLogContent.AgentWithdrawal_SuccessfulPaymentResponse,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal.Id,
                null,
                withdrawal.Amount);
        }

        protected internal override async Task HandleFailedPaymentResponse(string response)
        {
            withdrawal = await Fail(response, logDetails: response);
        }
        #endregion

        #region Callback
        public override async Task<PaymentCallbackResponse> Callback(PaymentCallbackRequestContext request)
        {
            try
            {
                var service = new AgentWithdrawalService();
                withdrawal = await service.GetByMerchantOrderNumber(handler.ExtractCallbackMerchantOrderNumber(), "PaymentChannel");
                if (withdrawal == null)
                {
                    throw new BusinessException(Constants.Messages.InvalidMerchantOrderNumber);
                }

                await OnCallbackRequestReceived(request.Content);

                if (!handler.IsValidCallback(withdrawal.PaymentChannel.MerchantKey))
                {
                    throw new BusinessException(Constants.Messages.InvalidSign);
                }

                switch (withdrawal.Status)
                {
                    case WithdrawalStatus.AutoPaymentSuccessful:
                    case WithdrawalStatus.ManualConfirmed:
                    case WithdrawalStatus.AutoPaymentManualConfirmedFailed:
                    case WithdrawalStatus.AutoPaymentFailed:
                        return await SucceedCallback();
                    case WithdrawalStatus.AutoPaymentInProgress:
                        break;
                    default:
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                withdrawal.Status));
                }

                if (handler.IsSuccessfulCallback())
                {
                    withdrawal = await service.AutoSucceed(withdrawal.Id);
                }
                else
                {
                    withdrawal = await service.AutoFail(withdrawal.Id, handler.GetCallbackErrorMessage(request.Content));
                }

                return await SucceedCallback();
            }
            catch (Exception ex)
            {
                await OnCallbackResponseSending(ex.ToString());

                return FailCallback(ex);
            }
        }

        internal override async Task OnCallbackRequestReceived(string callback)
        {
            await EntityLogService.Add(
                EntityLogContent.AgentWithdrawal_CallbackRequestReceived,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal.Id,
                callback,
                withdrawal.Amount);
        }

        internal override async Task OnCallbackResponseSending(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.AgentWithdrawal_CallbackResponseSending,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal?.Id,
                response,
                withdrawal.Amount);
        }

        protected override async Task<PaymentCallbackResponse> SucceedCallback()
        {
            var response = new AgentWithdrawalCallbackResponse
            {
                AgentWithdrawal = withdrawal,
                Content = handler.CallbackSuccess()
            };

            await OnCallbackResponseSending(response.Content);

            return response;
        }

        protected override PaymentCallbackResponse FailCallback(Exception exception)
        {
            var response = new AgentWithdrawalCallbackResponse
            {
                AgentWithdrawal = withdrawal,
                Content = exception.Message
            };

            return response;
        }
        #endregion

        #region Query
        internal override async Task OnQueryRequestSending(PaymentRequest request)
        {
            await EntityLogService.Add(
                EntityLogContent.AgentWithdrawal_QueryRequestSending,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal.Id,
                JsonConvert.SerializeObject(request),
                withdrawal.Amount);
        }

        internal override async Task OnQueryResponseReceived(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.AgentWithdrawal_QueryResponseReceived,
                EntityLogTargetType.AgentWithdrawal,
                withdrawal.Id,
                response,
                withdrawal.Amount);
        }

        protected internal override Task HandleSuccessfulQueryResponse(PaymentQueryRequestContext requestContext)
        {
            return new AgentWithdrawalService().AutoSucceed(withdrawal.Id);
        }

        protected internal override Task HandleFailedQueryResponse(string declineNotes)
        {
            return new AgentWithdrawalService().AutoFail(withdrawal.Id, declineNotes);
        }
        #endregion

        protected override async Task EnsureMerchantOrderNumber()
        {
            if (string.IsNullOrEmpty(withdrawal.MerchantOrderNumber))
            {
                withdrawal.MerchantOrderNumber = $"{Constants.MerchantOrderNumberPrefixes.AgentWithdrawal}{handler.EnsureMerchantOrderNumber()}";

                await new AgentWithdrawalService().Update(withdrawal);
            }
        }

        private async Task<AgentWithdrawal> Fail(string message, string customerMessage = null, string logDetails = null)
        {
            if (string.IsNullOrEmpty(customerMessage))
            {
                var bankCard = await new AgentService().GetBankCard(withdrawal.Username, withdrawal.PayeeCardNumber);
                if (bankCard?.Verified == true)
                {
                    customerMessage = Constants.Messages.GeneralFailure;
                }
                else
                {
                    customerMessage = string.Format(
                        Constants.MessageTemplates.WithdrawalFailure,
                        withdrawal.PayeeCardNumber,
                        withdrawal.PayeeName);
                }
            }

            return await new AgentWithdrawalService().AutoFail(withdrawal.Id, message, customerMessage, logDetails);
        }
    }
}
