﻿using GW.PC.Payment.SDK;
using System;
using System.Threading.Tasks;
using GW.PC.Payment.Manager;
using GW.PC.Models;
using GW.PC.Core;
using GW.PC.Payment;
using GW.PC.Services.EF;
using Newtonsoft.Json;
using GW.PC.Web.Core;
using GW.PC.Merchant.Gateway.Context;
using GW.PC.Models.Queues;
using GW.PC.Services.Queues.EF;

namespace GW.PC.Payment.Manager
{
    public class WithdrawalManager : HttpPaymentManager
    {
        private Models.Merchant merchant = null;
        private MerchantWithdrawal withdrawal = null;
        private MerchantRequestContext requestContext = null;

        private bool VerifyMerchantSign(string merchantKey)
        {
            if (requestContext is MerchantWithdrawalRequestContext withdrawalContext)
            {
                return withdrawalContext.Sign == VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(withdrawalContext, string.Empty, string.Empty), merchantKey);
            }

            var queryContext = (MerchantQueryRequestContext)requestContext;

            return queryContext.Sign == VerifyUtilities.AesEncrypt(PaymentUtilities.CreateSignRawText(queryContext, string.Empty, string.Empty), merchantKey);
        }

        private async Task<MerchantWithdrawal> CreateWithdrawal()
        {
            var requestContext = (MerchantWithdrawalRequestContext)this.requestContext;

            var requestedAmount = Convert.ToDecimal(requestContext.RequestedAmount);
            var paymentPlatform = (Models.PaymentPlatform)Enum.Parse(typeof(Models.PaymentPlatform), requestContext.PaymentPlatform);
            var payeeBankId = await new BankService().GetBankId(requestContext.BankCode);
            var channel = await new MerchantWithdrawalService().SelectWithdrawChannel(merchant.Id, requestedAmount, payeeBankId, paymentPlatform);

            if (channel == null)
            {
                throw new RequestException(Constants.Messages.NoWithdrawalChannel, NoticeType.PaymentCenter);
            }

            var withdrawal = new MerchantWithdrawal
            {
                SystemOrderNumber = await SystemOrderNumberService.Next(),
                Amount = requestedAmount,
                CreatedAt = DateTime.Now.ToE8(),
                PayeeBankId = payeeBankId,
                PayeeCardNumber = requestContext.PayeeCardNumber,
                PayeeName = requestContext.PayeeName,
                Status = WithdrawalStatus.AutoPaymentInProgress,
                Username = requestContext.Username,
                WithdrawalIP = requestContext.Ip,
                MerchantId = merchant.Id,
                PaymentChannelId = channel.Id,
                MerchantOrderNumber = requestContext.MerchantOrderNumber,
                PaymentCenterOrderNumber = $"{Constants.MerchantOrderNumberPrefixes.MerchantWithdrawal}{RandomGenerator.NewMerchantOrderNumber()}",
                CallbackUrl = requestContext.CallbackUrl,
            };

            withdrawal = await new MerchantWithdrawalService().Add(withdrawal);

            withdrawal.PaymentChannel = channel;

            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.MerchantWithdrawal_Created,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = EntityLogTargetType.MerchantWithdrawal,
                TargetId = withdrawal.Id,
                Details = withdrawal.Amount.ToString()
            });

            return withdrawal;
        }

        public async Task<WithdrawalManager> Init(MerchantRequestContext requestContext)
        {
            this.requestContext = requestContext;

            merchant = await new MerchantService().GetByUsername(requestContext.MerchantUsername);

            if (!VerifyMerchantSign(merchant.MerchantKey))
            {
                throw new RequestException(Constants.Messages.InvalidSign, NoticeType.PaymentCenter);
            }

            if (requestContext is MerchantWithdrawalRequestContext)
            {
                withdrawal = await new MerchantWithdrawalService().GetByMerchantOrderNumber(merchant.Id, requestContext.MerchantOrderNumber);

                if (withdrawal != null)
                {
                    throw new RequestException(Constants.Messages.OrderNumberDuplicated, NoticeType.PaymentCenter);
                }

                withdrawal = await CreateWithdrawal();

                var ppb = await new PaymentProviderBankService().GetByBankId(
                PayeeBankId.GetValueOrDefault(),
                PaymentChannel.ProviderId,
                PaymentType.Withdrawal,
                PaymentChannel.PaymentMethod);

                ProviderBankCode = ppb.Code;
                ProviderBankName = ppb.Name;
            }
            else
            {
                withdrawal = await new MerchantWithdrawalService().GetByMerchantOrderNumber(merchant.Id, requestContext.MerchantOrderNumber, "PaymentChannel.Provider");

                if (withdrawal == null)
                {
                    throw new RequestException(Constants.Messages.InvalidMerchantOrderNumber, NoticeType.PaymentCenter);
                }

            }

            Common.CheckNull(withdrawal?.PaymentChannel, nameof(withdrawal.PaymentChannel));

            withdrawal.PaymentChannel.Merchant = merchant;

            handler = await HttpPaymentHandlerFactory.CreateHandler(withdrawal.PaymentChannel.AssemblyName, withdrawal.PaymentChannel.HandlerType, this);

            return this;
        }

        public override async Task<HttpPaymentManager> Init(int paymentChannelId)
        {
            var config = await new PaymentChannelService().GetHandlerConfig(paymentChannelId);

            handler = await HttpPaymentHandlerFactory.CreateHandler(config.Item1, config.Item2, this);

            return this;
        }

        #region Set parameters
        internal override int? PayeeBankId => withdrawal.PayeeBankId;
        internal override string RequestedMerchantOrderNumber => withdrawal.MerchantOrderNumber;

        internal override HttpPayment BuildHttpPayment() => withdrawal?.BuildHttpPayment();

        internal override PaymentChannel PaymentChannel => withdrawal.PaymentChannel;
        #region Set payment channel's parameters

        public override string MerchantUsername => PaymentChannel.MerchantUsername;
        public override string MerchantKey => PaymentChannel.MerchantKey;
        public override string RequestUrl => PaymentChannel.RequestUrl;
        public override string CallbackUrl => PaymentChannel.CallbackUrl;
        public override string QueryUrl => PaymentChannel.QueryUrl;
        public override string Arguments => PaymentChannel.Arguments;

        #endregion

        public override string MerchantOrderNumber => withdrawal.PaymentCenterOrderNumber;

        public override string PayeeCardNumber => withdrawal.PayeeCardNumber;

        public override string PayeeName => withdrawal.PayeeName;

        public override decimal RequestedAmount => withdrawal.Amount;

        private string ProviderBankCode { get; set; }
        public override string BankCode => ProviderBankCode;

        private string ProviderBankName { get; set; }
        public override string BankName => ProviderBankName;

        public override string Username => withdrawal.Username;

        public override string PaymentIP => withdrawal.WithdrawalIP;

        #endregion

        #region Payment
        internal override async Task OnPaymentRequestSending(PaymentRequestContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.MerchantWithdrawal_PaymentRequestSending,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = EntityLogTargetType.MerchantWithdrawal,
                TargetId = withdrawal.Id,
                Details = JsonConvert.SerializeObject(context)
            });
        }

        internal override async Task OnPaymentHttpRequestSending(PaymentGatewayRequestContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.MerchantWithdrawal_PaymentHttpRequestSending,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = EntityLogTargetType.MerchantWithdrawal,
                TargetId = withdrawal.Id,
                Details = JsonConvert.SerializeObject(context).GetSubstring(500)
            });
        }

        internal override async Task OnPaymentResponseReceived(string response)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.MerchantWithdrawal_PaymentResponseReceived,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = EntityLogTargetType.MerchantWithdrawal,
                TargetId = withdrawal.Id,
                Details = response.Substring(0, (response.Length > 2000 ? 2000 : response.Length))
            });
        }

        internal override async Task OnPaymentResponseParse(PaymentResponseContext context)
        {
            await new EntityLogService().Add(new EntityLog
            {
                Content = EntityLogContent.MerchantWithdrawal_PaymentResponseParse,
                CreatedAt = DateTime.Now.ToE8(),
                TargetType = EntityLogTargetType.MerchantWithdrawal,
                TargetId = withdrawal.Id,
                Details = JsonConvert.SerializeObject(context)
            });
        }

        protected internal override async Task OnPaymentException(Exception ex)
        {
            await OnBadRequestException(ex);
            await EntityLogService.Add(
                EntityLogContent.MerchantWithdrawal_PaymentBadRequestException,
                EntityLogTargetType.MerchantWithdrawal,
                withdrawal.Id,
                null,
                withdrawal.Amount);
        }

        private async Task OnBadRequestException(Exception ex)
        {
            if (ex.GetBaseException() is TaskCanceledException baseException)
            {
                withdrawal.DeclineNotes = $"{Constants.Messages.ServerResponseFailure}:{baseException.Message}";
            }
            else
            {
                withdrawal.DeclineNotes = $"{Constants.Messages.HandlerFailure}:{ex.Message}";
            }

            withdrawal = await new MerchantWithdrawalService().Update(withdrawal);
        }

        protected internal override async Task HandleSuccessfulPaymentResponse()
        {
            withdrawal.PaymentOrderNumber = handler.plugin.PaymentResponseContent.PaymentOrderNumber;

            withdrawal = await new MerchantWithdrawalService().Update(withdrawal);

            await EntityLogService.Add(
                EntityLogContent.MerchantWithdrawal_SuccessfulPaymentResponse,
                EntityLogTargetType.MerchantWithdrawal,
                withdrawal.Id,
                null,
                withdrawal.Amount);

            var queueContext = new PaymentQueryRequest
            {
                RequestId = Guid.NewGuid().ToString(),
                MerchantUsername = merchant.Username,
                PaymentType = PaymentChannel.PaymentType,
                PaymentMethod = PaymentChannel.PaymentMethod,
                MerchantOrderNumber = withdrawal.MerchantOrderNumber,
                CreatedAt = DateTime.Now.ToE8(),
                NextSendingAt = DateTime.Now.ToE8()
            };

            await new PaymentQueryRequestService().Add(queueContext);
        }

        protected internal override async Task HandleFailedPaymentResponse(string response)
        {
            withdrawal = await Fail(
                response.Length > 500 ? response.Substring(0, 500) : response
                , logDetails: response);
        }
        #endregion

        #region Callback
        public override async Task<PaymentCallbackResponse> Callback(PaymentCallbackRequestContext request)
        {
            try
            {
                var parseContext = handler.plugin.ReadCallbackRequest(request.Content);

                handler.plugin.CallbackRequestContent = parseContext.Content;

                var service = new MerchantWithdrawalService();
                withdrawal = await service.GetByPaymentCenterOrderNumber(handler.plugin.ExtractCallbackMerchantOrderNumber(), "PaymentChannel.Provider", "Merchant");
                if (withdrawal == null)
                {
                    return SkipCallback(Constants.Messages.InvalidMerchantOrderNumber);
                }

                await OnCallbackRequestReceived(request.Content);

                if (!handler.plugin.IsValidCallback(withdrawal.PaymentChannel.MerchantKey))
                {
                    throw new BusinessException(Constants.Messages.InvalidSign);
                }

                switch (withdrawal.Status)
                {
                    case WithdrawalStatus.AutoPaymentSuccessful:
                    case WithdrawalStatus.ManualConfirmed:
                    case WithdrawalStatus.Declined:
                    case WithdrawalStatus.AutoPaymentManualConfirmedFailed:
                    case WithdrawalStatus.AutoPaymentFailed:
                        return SkipCallback();
                    case WithdrawalStatus.AutoPaymentInProgress:
                        break;
                    default:
                        throw new BusinessException(
                            string.Format(
                                Constants.MessageTemplates.InvalidCurrentStatus,
                                withdrawal.Status));
                }

                if (handler.plugin.IsSuccessfulCallback())
                {
                    withdrawal = await service.AutoSucceed(withdrawal.Id);
                }
                else
                {
                    withdrawal = await service.AutoFail(withdrawal.Id, handler.plugin.GetCallbackErrorMessage(request.Content));
                }

                await AddMerchantCallbackQueue();

                return await SucceedCallback();
            }
            catch (Exception ex)
            {
                await OnCallbackResponseSending(ex.ToString());

                return FailCallback(ex);
            }
        }

        public override async Task AddMerchantCallbackQueue()
        {
            var queue = new MerchantCallbackRequest
            {
                RequestId = Guid.NewGuid().ToString(),
                MerchantCallbackUrl = withdrawal.CallbackUrl,
                MerchantUsername = PaymentChannel.Merchant.Username,
                MerchantOrderNumber = withdrawal.MerchantOrderNumber,
                PaymentCenterOrderNumber = withdrawal.PaymentCenterOrderNumber,
                PaymentOrderNumber = withdrawal.PaymentOrderNumber,
                PaymentType = PaymentChannel.PaymentType,
                PaymentMethod = PaymentChannel.PaymentMethod,
                RequestedAmount = withdrawal.Amount.ToString(),
                ActualAmount = withdrawal.Amount.ToString(),
                SendCount = 0,
                CreatedAt = DateTime.Now.ToE8(),
                NextSendingAt = DateTime.Now.ToE8()
            };

            switch (withdrawal.Status)
            {
                case WithdrawalStatus.AutoPaymentSuccessful:
                case WithdrawalStatus.ManualConfirmed:
                case WithdrawalStatus.RetryAutoPaymentSuccessful:
                case WithdrawalStatus.RetryManualConfirmed:

                    queue.Status = TransactionStatus.Success;
                    break;
                case WithdrawalStatus.AutoPaymentFailed:
                case WithdrawalStatus.AutoPaymentManualConfirmedFailed:
                case WithdrawalStatus.FailedAndClosed:

                    queue.Status = TransactionStatus.Fail;
                    break;
                default:
                    queue.Status = TransactionStatus.UnKnow;
                    break;
            }

            await new MerchantCallbackRequestService().Add(queue);
        }

        internal override async Task OnCallbackRequestReceived(string callback)
        {
            await EntityLogService.Add(
                EntityLogContent.MerchantWithdrawal_CallbackRequestReceived,
                EntityLogTargetType.MerchantWithdrawal,
                withdrawal.Id,
                callback,
                withdrawal.Amount);
        }

        internal override async Task OnCallbackResponseSending(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.MerchantWithdrawal_CallbackResponseSending,
                EntityLogTargetType.MerchantWithdrawal,
                withdrawal?.Id,
                response,
                withdrawal?.Amount);
        }

        protected override PaymentCallbackResponse SkipCallback(string reason = "")
        {
            var response = new MerchantWithdrawalCallbackResponse
            {
                PaymentCallbackResult = PaymentCallbackResult.Skipped,
                Content = string.IsNullOrEmpty(reason) ? handler.plugin.CallbackSuccess() : reason
            };

            return response;
        }

        protected override async Task<PaymentCallbackResponse> SucceedCallback()
        {
            var response = new MerchantWithdrawalCallbackResponse
            {
                MerchantWithdrawal = withdrawal,
                Content = handler.plugin.CallbackSuccess(),
                PaymentCallbackResult = PaymentCallbackResult.Successful
            };

            await OnCallbackResponseSending(response.Content);

            return response;
        }

        protected override PaymentCallbackResponse FailCallback(Exception exception)
        {
            var response = new MerchantWithdrawalCallbackResponse
            {
                MerchantWithdrawal = withdrawal,
                Content = exception.Message,
                Exception = exception
            };

            return response;
        }
        #endregion

        #region Query
        internal override async Task OnQueryRequestSending(PaymentRequestContext request)
        {
            await EntityLogService.Add(
                EntityLogContent.MerchantWithdrawal_QueryRequestSending,
                EntityLogTargetType.MerchantWithdrawal,
                withdrawal.Id,
                JsonConvert.SerializeObject(request),
                withdrawal.Amount);
        }

        internal override async Task OnQueryResponseReceived(string response)
        {
            await EntityLogService.Add(
                EntityLogContent.MerchantWithdrawal_QueryResponseReceived,
                EntityLogTargetType.MerchantWithdrawal,
                withdrawal.Id,
                response,
                withdrawal.Amount);
        }

        protected internal override async Task HandleSuccessfulQueryResponse()
        {
            if (withdrawal.Status == WithdrawalStatus.AutoPaymentInProgress)
            {
                withdrawal = await new MerchantWithdrawalService().AutoSucceed(withdrawal.Id);
            }

            await AddMerchantCallbackQueue();
        }

        protected internal override async Task HandleFailedQueryResponse(string declineNotes)
        {
            if (withdrawal.Status == WithdrawalStatus.AutoPaymentInProgress)
            {
                withdrawal = await new MerchantWithdrawalService().AutoFail(withdrawal.Id, declineNotes);
            }

            await AddMerchantCallbackQueue();
        }
        #endregion

        protected override async Task EnsureMerchantOrderNumber()
        {
            if (string.IsNullOrEmpty(withdrawal.PaymentCenterOrderNumber))
            {
                withdrawal.PaymentCenterOrderNumber = $"{Constants.MerchantOrderNumberPrefixes.MerchantWithdrawal}{handler.EnsureMerchantOrderNumber()}";

                await new MerchantWithdrawalService().Update(withdrawal);
            }
        }

        private async Task<MerchantWithdrawal> Fail(string message, string customerMessage = null, string logDetails = null)
        {
            customerMessage = string.Format(
                         Constants.MessageTemplates.WithdrawalFailure,
                         withdrawal.PayeeCardNumber,
                         withdrawal.PayeeName);

            return await new MerchantWithdrawalService().AutoFail(withdrawal.Id, message, customerMessage, logDetails);
        }
    }
}
