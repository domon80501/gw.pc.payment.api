﻿using GW.PC.Payment.SDK;
using GW.PC.Payment.Manager;
using System;
using System.Reflection;
using System.Threading.Tasks;
using GW.PC.Core;
using System.IO;
using System.Web;

namespace GW.PC.Payment
{
    internal static class HttpPaymentHandlerFactory
    {
        internal static string path;

        static HttpPaymentHandlerFactory()
        {
            path = HttpContext.Current.Server.MapPath(Constants.AppSettings.Get(AppSettingNames.PaymentAssemblyPath));
        }

        internal static async Task<HttpPaymentHandler> CreateHandler(string assemblyName, string handlerFullName, HttpPaymentManager manager)
        {
            if (string.IsNullOrEmpty(assemblyName) || string.IsNullOrEmpty(handlerFullName))
            {
                return null;
            }

            var guid = Guid.NewGuid().ToString();

            var setup = AppDomain.CurrentDomain.SetupInformation;
            var domain = AppDomain.CreateDomain($"{guid}|{assemblyName}|{handlerFullName}", AppDomain.CurrentDomain.Evidence, setup);
            var obj = domain.CreateInstance(typeof(AssemblyLoader).Assembly.FullName, typeof(AssemblyLoader).FullName);
            var loader = (AssemblyLoader)obj.Unwrap();

            var plugin = loader.Initialize(Path.Combine(path, $"{assemblyName}.dll"), handlerFullName);

            var handler = new HttpPaymentHandler();

            await handler.Initialize(domain, plugin, manager);

            return handler;
        }

        internal class AssemblyLoader : MarshalByRefObject
        {
            public PaymentHandlerSDK Initialize(string path, string handlerFullName)
            {
                if (!File.Exists(path))
                {
                    throw new ArgumentException($"Unable to load the assembly: {path}");
                }

                var assembly = Assembly.Load(AssemblyName.GetAssemblyName(path));

                if (!(assembly.CreateInstance(handlerFullName, true) is PaymentHandlerSDK plugin))
                {
                    throw new ArgumentException($"Unable to create the handler with type: {handlerFullName}");
                }

                return plugin;
            }
        }
    }
}