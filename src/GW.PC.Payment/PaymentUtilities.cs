﻿using GW.PC.Payment.SDK;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GW.PC.Payment
{
    public static class PaymentUtilities
    {
        /// <summary>
        /// 建立待签原文字串
        /// </summary>
        /// <param name="content"></param>
        /// <param name="keyPropertyName">金钥的参数名称，独立填写连结符号</param>
        /// <param name="merchantKey">金钥</param>
        /// <param name="orderBy">是否需要排序，预设true</param>
        /// <param name="valueExist">值存在才加入签名，预设true</param>
        /// <param name="symbol">指定的连结符号，预设 { = , & } </param>
        /// <returns></returns>
        public static string CreateSignRawText(object content, string keyPropertyName, string merchantKey
             , bool orderBy = true, bool valueExist = true, bool includePropertyName = true, params string[] symbol)
        {
            symbol = symbol.Length != 2 ? new string[] { "=", "&" } : symbol;

            var props = content.GetType().GetProperties()
                .Where(o => !o.GetCustomAttributes(typeof(SignIgnoreAttribute), false).Any()
                && (valueExist ? !string.IsNullOrEmpty(o.GetValue(content)?.ToString()) : !valueExist));
            props = orderBy ? props.OrderBy(o => o.CustomAttributes.Single().ConstructorArguments.Single().Value) : props;

            string[] signArray = props
                .Select(o => $"{(includePropertyName ? o.CustomAttributes.Single().ConstructorArguments.Single().Value : string.Empty)}{symbol[0]}{o.GetValue(content)}").ToArray();

            return string.Join(symbol[1], signArray) + $"{keyPropertyName}{merchantKey}";
        }

        /// <summary>
        /// 取得md5加密签名
        /// </summary>
        /// <param name="input">待签原文字串</param>
        /// <param name="lowercase">转换大小写，预设小写</param>
        /// <returns></returns>
        public static string GetMd5(string input, bool lowercase = true)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException(nameof(input));
            }

            var result = BitConverter.ToString(
                MD5.Create().ComputeHash(
                    Encoding.UTF8.GetBytes(input))).Replace("-", "");

            return lowercase ? result.ToLower() : result;
        }

        /// <summary>
        /// 取得hmac-md5加密签名        
        /// </summary>
        /// <param name="input">待签原文字串</param>
        /// <param name="key">hash密钥</param>
        /// <param name="lowercase">转换大小写，预设小写</param>
        /// <returns></returns>
        public static string GetHmacMd5(string input, string key, bool lowercase = true)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException(nameof(input));
            }
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentNullException(nameof(key));
            }

            var hmacMD5 = new HMACMD5(Encoding.UTF8.GetBytes((key)));

            var result = BitConverter.ToString(
                hmacMD5.ComputeHash(
                    Encoding.UTF8.GetBytes(input))).Replace("-", "");

            return lowercase ? result.ToLower() : result;
        }

        /// <summary>
        /// 转换 x-www-form-urlencoded 格式字串为指定 T 型别
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="text"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static T DeserializeQueryString<T>(string text, string separator = "&")
            where T : new()
        {
            var result = new T();

            var properties = typeof(T).GetProperties();
            var items = text.Split(new string[] { separator }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in items)
            {
                var pieces = item.Split('=');
                var key = pieces[0];
                var value = pieces[1];

                foreach (var property in properties)
                {
                    if (property.GetCustomAttributes(typeof(JsonPropertyAttribute), false).SingleOrDefault()
                        is JsonPropertyAttribute attr)
                    {
                        if (attr.PropertyName.ToLower() == key.ToLower())
                        {
                            property.SetValue(result, value);
                        }
                    }
                    else
                    {
                        if (property.Name.ToLower() == key.ToLower())
                        {
                            property.SetValue(result, value);
                        }
                    }
                }
            }

            return result;
        }
    }
}
