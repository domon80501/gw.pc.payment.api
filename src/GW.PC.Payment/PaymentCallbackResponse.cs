﻿using System;

namespace GW.PC.Payment
{
    public class PaymentCallbackResponse
    {
        public string Content { get; set; }
        public Exception Exception { get; set; }
        public PaymentCallbackResult PaymentCallbackResult { get; set; }
    }
}
