﻿namespace GW.PC.Payment
{
    public enum PaymentCallbackResult
    {
        None,
        Successful,
        Failed,
        Skipped
    }
}
