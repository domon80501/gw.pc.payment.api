﻿using GW.PC.Merchant.Gateway.Context;
using System.Net.Http;
using System.Threading.Tasks;

namespace GW.PC.Payment.Api.Client
{
    public class WithdrawalsClient : PaymentCenterWebApiClient
    {
        public WithdrawalsClient()
        {
            RoutePrefix = "api";
        }

        public Task<HttpResponseMessage> Query(MerchantQueryRequestContext context)
        {
            return GetAsync("withdrawals", context);
        }
        public Task<HttpResponseMessage> Request(MerchantWithdrawalRequestContext context)
        {
            return PostAsync("withdrawals", context);
        }
    }
}
