﻿using GW.PC.Merchant.Gateway.Context;
using System.Net.Http;
using System.Threading.Tasks;

namespace GW.PC.Payment.Api.Client
{
    public class DepositsClient : PaymentCenterWebApiClient
    {
        public DepositsClient()
        {
            RoutePrefix = "api";
        }

        public Task<HttpResponseMessage> Query(MerchantQueryRequestContext context)
        {
            return GetAsync("deposits", context);
        }
        public Task<HttpResponseMessage> Request(MerchantDepositRequestContext context)
        {   
            return PostAsync("deposits", context);
        }
    }
}
