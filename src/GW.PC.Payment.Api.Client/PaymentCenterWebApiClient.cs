﻿using GW.PC.Core;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace GW.PC.Payment.Api.Client
{
    public abstract class PaymentCenterWebApiClient
    {
        protected HttpClient client = null;
        protected string RoutePrefix = null;

        public PaymentCenterWebApiClient()
        {
            client = new HttpClient
            {
                BaseAddress = new Uri(Constants.AppSettings.Get(
                    AppSettingNames.PaymentCenterWebApiAddress))
            };
        }

        protected async Task<TResult> GetAsync<TModel, TResult>(string action, TModel model, bool prefix = true)
        {
            var uri = prefix ? $"{RoutePrefix}/{action}?{model.SerializeToQueryString()}"
                : $"{action}?{model.SerializeToQueryString()}";
            var response = await client.GetAsync(uri);

            return await response.Content?.ReadAsAsync<TResult>();
        }

        protected async Task<TResult> GetAsync<TResult>(string action, bool prefix = true)
        {
            var uri = prefix ? $"{RoutePrefix}/{action}" : action;
            var response = await client.GetAsync(uri);

            return await response.Content?.ReadAsAsync<TResult>();
        }

        protected async Task<TResult> PostAsJsonAsync<TModel, TResult>(string uri, TModel model, bool prefix = true)
        {
            uri = prefix ? $"{RoutePrefix}/{uri}" : uri;
            var response = await client.PostAsJsonAsync(uri, model);

            return await response.Content?.ReadAsAsync<TResult>();
        }

        protected async Task<HttpResponseMessage> GetAsync<TModel>(string action, TModel model, bool prefix = true)
        {
            var uri = prefix ? $"{RoutePrefix}/{action}?{model.SerializeToQueryString()}"
                : $"{action}?{model.SerializeToQueryString()}";

            return await client.GetAsync(uri);
        }

        protected async Task<HttpResponseMessage> PostAsync<TModel>(string uri, TModel model, bool prefix = true)
        {
            uri = prefix ? $"{RoutePrefix}/{uri}" : uri;

            return await client.PostAsJsonAsync(uri, model);
        }
    }
}
