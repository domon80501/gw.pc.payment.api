﻿using GW.PC.Core;
using GW.PC.Payment.Context;
using GW.PC.Web.Core;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace GW.PC.Payment.Api.Client
{
    public class DepositCallbacksClient : WebApiClientBase
    {
        protected static HttpClient client = null;

        public DepositCallbacksClient()
        {
            client = client ?? new HttpClient
            {
                BaseAddress = new Uri(Constants.AppSettings.Get(
                    AppSettingNames.PaymentCenterWebApiAddress))
            };

            RoutePrefix = "api";
        }

        protected override HttpClient GetHttpClient()
        {
            return client;
        }

        public Task<PaymentCallbackResponseContext> Callback(PaymentCallbackRequestContext context)
        {
            return PostAsJsonAsyncRawResult<PaymentCallbackRequestContext, PaymentCallbackResponseContext>("depositcallbacks", context);
        }
    }
}
